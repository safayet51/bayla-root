const express = require("express");
const colors = require("colors");
const cors = require("cors");
const app = express();
const port = process.env.PORT || 5000;
require("dotenv").config();
const ATLAS_URI = process.env.MONGO_URI;
const { MongoClient } = require("mongodb");
const { api } = require("./app.controller");
const aws = require("aws-sdk");

const multer = require("multer");
const multerS3 = require("multer-s3");

const spacesEndpoint = new aws.Endpoint("sgp1.digitaloceanspaces.com");
const s3 = new aws.S3({
  secretAccessKey: "ZEgkDdfBtROQBmjIEhoBuWSBaPF9/LYhntTdOVb7YfA",
  accessKeyId: "DO00DEFBZ37QLY9EBPXX",
  Bucket: "baylaorg",
  endpoint: spacesEndpoint,
  acl: "public-read",
});

const connectionString = ATLAS_URI;
const client = new MongoClient(connectionString, { useUnifiedTopology: true });

// Middlewares
app.use(cors());
app.use(express.json({ limit: "50mb" }));

// MongoDB Connection
try {
  client
    .connect()
    .then((database) => {
      const db = database.db("bayla");
      global.db = db;
      console.log("Mongo connected".bgGreen.white);
    })
    .catch((e) => {
      console.error(e);
    });
} catch (e) {
  console.error(e);
}

// Multer Configuration
const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: "baylaorg",
    acl: "public-read",
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString() + "_" + file.originalname);
    },
  }),
});

// API Endpoints
app.post("/baylaapi/app", api);

app.post(
  "/baylaapi/attachment",
  upload.array("files", 12),
  async (req, res) => {
    console.log("req", req);
    try {
      return res.send(req.files);
    } catch (error) {
      return res.send(error.message);
    }
  }
);

app.get("/baylaapi", (req, res) => {
  res.send("Bayla successfully running".bgBlue.white);
});

// Server Start
app.listen(port, () => {
  console.log(`Project running on port ${port}`.bgYellow.white);
});
