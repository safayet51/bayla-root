// /** @type {import('next').NextConfig} */
// const nextConfig = {
//   images: {
//     remotePatterns: [
//       {
//         protocol: "http",
//         hostname: "128.199.168.182",
//       },
//     ],
//   },
//   // webpack: (config) => {
//   //   config.module.rules.push({
//   //     test: /\.node/,
//   //     use: "raw-loader",
//   //   });
//   //   return config;
//   // },

//   rules: [
//     // other rules...

//     {
//       test: /\.(pdf)$/,
//       use: [
//         {
//           loader: "file-loader",
//           options: {
//             name: "[name].pdf",
//             outputPath: "assets/publications/",
//           },
//         },
//       ],
//     },
//   ],
// };

// module.exports = nextConfig;

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: "bayla.org",
      },
      {
        protocol: "https",
        hostname: "bayla.org",
      },
      {
        protocol: "https",
        hostname: "baylaorg.sgp1.digitaloceanspaces.com",
      },
    ],
  },
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(pdf)$/,
      use: [
        {
          loader: "file-loader",
          options: {
            name: "[name].pdf",
            outputPath: "assets/publications/",
          },
        },
      ],
    });

    return config;
  },
};

module.exports = nextConfig;
