"use client";

import Layout from "@/components/shared/Layout";

function AdminLayout({ children }) {
  return (
    <div className="">
      <Layout>{children}</Layout>
    </div>
  );
}

export default AdminLayout;
