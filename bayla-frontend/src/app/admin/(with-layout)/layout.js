"use client";

import Side from "@/components/shared/Side";

function WithAdminLayout({ children }) {
  return (
    <div className="flex">
      <Side />
      <div className="px-4 w-full">{children}</div>
    </div>
  );
}

export default WithAdminLayout;
