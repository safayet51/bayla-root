"use client";

import axios from "axios";
import { useEffect, useState } from "react";
import { MdEmail, MdLocalPhone } from "react-icons/md";

const PromoteDetails = ({ params }) => {
  const [singleData, setSingledata] = useState();
  const Id = params.id;
  useEffect(() => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "read",
        collection: "promote",
        data: {},
        params: { _id: Id },
      })
      .then((res) => {
        setSingledata(res.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);
  console.log("first", singleData);
  return (
    <div className="text-[17px] grid grid-cols-3 gap-2 px-10 mt-20">
      <div className="font-semibold text-[17px] space-y-2">
        <p>Name:</p>
        <p>Company Name:</p>
        <p>Email:</p>
        <p>Mobile:</p>
        <p>Selected Audiences:</p>
        <p>Promote Status:</p>
        <p>Paid Status:</p>
      </div>
      <div className="col-span-2 space-y-2">
        <p>{singleData?.displayName}</p>
        <p>{singleData?.companyName}</p>
        <p>{singleData?.email}</p>
        <p>{singleData?.mobileNumber}</p>
        <p>{singleData?.audiences}K</p>
        <select className="bg-white rounded-[4px] border border-blue-gray-900 px-2 py-1">
          <option>Inque</option>
          <option>Promoting</option>
          <option> Complete</option>
        </select>

        <div className="flex justify-normal items-start gap-5">
          <div className="h-48 w-72">
            <button className="bg-blue-500 px-2.5 rounded-md text-white">
              Amount
            </button>
            <p>{singleData?.totalSelected} BDT</p>
          </div>

          <div className="h-48 w-72">
            <button className="bg-green-500 px-2.5 rounded-md text-white">
              Paid
            </button>
          </div>
          <div className="h-48 w-72">
            <button className="bg-red-500 px-2.5 rounded-md text-white">
              Due
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PromoteDetails;
