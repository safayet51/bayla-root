"use client";

import { useEffect, useState } from "react";
import DataTable from "react-data-table-component";

import dynamic from "next/dynamic";
import axios from "axios";
import moment from "moment";
import Link from "next/link";
import { FaEye } from "react-icons/fa";
const PromotePage = () => {
  const [promoteData, setpromoteData] = useState([]);
  const [toggole, setToggle] = useState(1);
  useEffect(() => {
    console.log(toggole);
  }, [toggole]);

  async function fetchData() {
    console.log("hi1");
    const res = await axios.post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
      action: "get",
      collection: "promote",
      data: {},
      params: {},
    });
    const data = await res.data;
    // fetchData();
    setpromoteData(data);
    console.log("promote data", data);
  }
  useEffect(() => {
    fetchData();
  }, []);

  const columns = [
    {
      name: "Serial No.",
      selector: (_, index) => index + 1,
    },
    {
      name: "Name",
      selector: (row) => row.displayName,
    },
    {
      name: "Email",
      selector: (row) => row.email,
    },
    {
      name: "Mobile Number",
      selector: (row) => row.mobileNumber,
    },
    {
      name: "Company Name",
      selector: (row) => row.companyName,
    },
    {
      name: "Date",
      selector: (row) => moment(row.date).format("ll"),
    },

    {
      name: "Promote Status",
      selector: (row) => row.audiences,
    },
    {
      name: "Payment Status",
      selector: (row) => row.audiences,
    },
    {
      name: "View",
      // selector: row => row.img,
      cell: (row) => (
        <div className="flex justify-center items-center gap-6 text-[18px]">
          <Link href={`/admin/promote/${row._id}`}>
            <FaEye className="cursor-pointer" />
          </Link>
        </div>
      ),
    },
  ];

  return (
    <div>
      <div className="mx-20 mb-5 overflow-auto h-[80vh] mt-20">
        {promoteData && (
          <DataTable columns={columns} data={promoteData} pagination />
        )}
      </div>
    </div>
  );
};

export default dynamic(() => Promise.resolve(PromotePage), { ssr: false });
