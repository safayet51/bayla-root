"use client";

import ContentEditPageForId from "@/components/UI/AdminPageUi/ContentEditPageForId";

const ContentDetailPage = ({ params = { id: 0 } }) => {
  return (
    <div>
      <ContentEditPageForId Id={params?.id || 0} />
    </div>
  );
};

export default ContentDetailPage;
