"use client";

import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import axios from "axios";
import Image from "next/image";
import { IoMdAdd } from "react-icons/io";
import { RiEditLine } from "react-icons/ri";
import deleteImg from "../../../../assets/delete-svgrepo-com.svg";
import Link from "next/link";
import ContentEditPage from "@/components/UI/AdminPageUi/ContentEditPage";

const DataTableClient = dynamic(() => import("react-data-table-component"), {
  ssr: false,
});

const ContentPage = () => {
  const [contentData, setContentData] = useState([]);
  const [toggole, setToggle] = useState(1);
  useEffect(() => {
    console.log(toggole);
  }, [toggole]);

  async function fetchData() {
    console.log("hi1");
    const res = await axios.post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
      action: "get",
      collection: "blog",
      data: {},
      params: {},
    });
    const data = await res.data;
    // fetchData();
    setContentData(data);
    console.log("data", data);
  }
  useEffect(() => {
    fetchData();
    console.log("hi", document.title);
  }, []);

  const handleDelete = async (id) => {
    async function DeleteByID() {
      const res = await axios.post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "delete",
        collection: "blog",
        data: {},
        params: { _id: id },
      });
      const data = await res.data;
      fetchData();
    }
    DeleteByID();
  };

  const handleStatusUpdate = async (id, status) => {
    async function handleStatusFunc() {
      const res = await axios.post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "update",
        collection: "blog",
        data: { status: !status },
        params: { _id: id },
      });
      const data = await res.data;
      fetchData();
    }
    handleStatusFunc();
  };

  const columns = [
    {
      name: "Serial No.",
      selector: (_, index) => index + 1,
    },
    {
      name: "Title",
      selector: (row) => row.title,
    },
    {
      name: "Image",

      cell: (row) => (
        <div>
          <Image src={row.filename} width={60} height={70} alt="Player" />
        </div>
      ),
    },

    {
      name: "Action",

      cell: (row) => (
        <div className="flex justify-center items-center gap-6 text-[18px]">
          <Link href={`/admin/content/${row?._id || 0}`}>
            {" "}
            <RiEditLine className="cursor-pointer" />
          </Link>
          <Image
            src={deleteImg}
            alt=""
            width={20}
            onClick={() => handleDelete(row._id)}
          />
        </div>
      ),
    },
    {
      name: "Status",

      cell: (row) => (
        <div>
          <p
            onClick={() => handleStatusUpdate(row._id, row.status)}
            className="cursor-pointer">
            {row?.status == true ? "Yes" : "No"}
          </p>
        </div>
      ),
    },
  ];

  return (
    <div>
      <div className="mt-10">
        {toggole == 2 ? (
          <ContentEditPage setToggle={setToggle} toggole={toggole} />
        ) : (
          <div className="mx-20 mb-5 overflow-auto h-[80vh]">
            <div className="relative py-12">
              <div className="absolute top-12 right-7">
                <button
                  className="bg-[#4E9345] px-3 py-1.5 rounded-md flex justify-center items-center gap-1 text-white"
                  onClick={() => setToggle(2)}>
                  Create Contents <IoMdAdd className="text-xl" />
                </button>
              </div>
            </div>
            {contentData && (
              <DataTableClient
                columns={columns}
                data={contentData}
                pagination
              />
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default dynamic(() => Promise.resolve(ContentPage), { ssr: false });
