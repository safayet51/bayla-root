"use client";
import { useSession, signIn, signOut } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FiLogOut } from "react-icons/fi";

const AdminPage = () => {
  const { data: session } = useSession();
  const router = useRouter();
  return (
    <div className="flex flex-col justify-center items-center my-32">
      {!session && (
        <div className="flex flex-col justify-center items-center">
          <p className="text-xl font-semibold">
            Want to go to Admin route? Please click the Admin button.
          </p>
          <button
            className="px-3 py-1 bg-[#4E9345] rounded-md text-white mt-3"
            onClick={() => {
              signIn();
            }}>
            Admin
          </button>
        </div>
      )}

      {session && (
        <div className="flex flex-col justify-center items-center">
          <div className="flex justify-center items-center gap-1">
            <p className="text-xl font-semibold">Go to</p>
            <Link href={"/admin/content"}>
              <button className="px-3 py-1 bg-[#4E9345] rounded-md text-white">
                Admin Dashboard
              </button>
            </Link>
          </div>
          <button
            className="px-3 py-1 border border-[#4E9345] rounded-md flex justify-center items-center gap-1 mt-3 text-sm"
            onClick={() => {
              signOut();
            }}>
            <FiLogOut />
            Logout
          </button>
        </div>
      )}
    </div>
  );
};

export default AdminPage;
