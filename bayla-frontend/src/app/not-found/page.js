import React from "react";

const NotFound = () => {
  return (
    <div className="text-center">
      <p className="text-3xl font-bold">404</p>
      <p>Not found</p>
    </div>
  );
};

export default NotFound;
