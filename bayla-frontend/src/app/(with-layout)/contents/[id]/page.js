"use client";
import {
  FaFacebookSquare,
  FaLinkedin,
  FaTwitterSquare,
  FaWhatsappSquare,
} from "react-icons/fa";
import { MdOutlineArrowBack } from "react-icons/md";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import axios from "axios";

const ContentsDetails = ({ params: { id } }) => {
  const [singleData, setSingleData] = useState();
  // useEffect(() => {
  //   axios
  //     .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
  //       action: "get",
  //       collection: "blog",
  //       data: {},
  //       params: {},
  //     })
  //     .then((res) => {
  //       res.data?.filter((e) => e._id === id && setSingleData(e));
  //       console.log(res.data);
  //     });
  // }, []);
  // console.log("singleData", singleData);

  useEffect(() => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "get",
        collection: "blog",
        data: {},
        params: {},
      })
      .then((res) => {
        // Use optional chaining to handle undefined
        const data = res.data?.find((e) => e._id === id);
        setSingleData(data);
        console.log(res.data);
      });
  }, [id]); // Include id in the dependency array

  console.log("singleData", singleData);
  return (
    <div className="overflow-x-hidden">
      <div className="relative w-[100vw] h-[80vh]">
        <Image
          src={singleData?.filename}
          fill={true}
          alt={"Background Image"}
        />
        <Link href="/contents">
          <div
            className="bg-white absolute top-10 left-10 z-[1] px-2 py-0.5 rounded-[4px]"
            style={{
              transform: "translate(-50%, -50%)",
              boxShadow: "0 0 10px rgba(0, 0, 0, 0.25)",
            }}>
            <h1 className="text-xl text-black">
              <MdOutlineArrowBack />
            </h1>
          </div>
        </Link>
        <div
          className="bg-[#4E9345] absolute top-10 right-2 z-[1] px-4 py-0.5 rounded-lg"
          style={{
            transform: "translate(-50%, -50%)",
            boxShadow: "0 0 10px rgba(0, 0, 0, 0.25)",
          }}>
          <button className="text-white bg-[#4E9345] font-semibold px-2 py-1">
            Promote
          </button>
        </div>
      </div>
      <div className="max-w-6xl mx-auto font-normal text-black">
        <h3 className="font-bold text-[45px] text-black pt-16 leading-[52px]">
          {singleData?.title}
        </h3>
        {/* <p className="py-6 text-justify">Bangladeshi Apparel manufacturers are heavily investing to meet their Environmental, Social, and Governance- ESG commitments and ensure sustainable growth. The industry continues to attract substantial investments, with over $1.1 billion recently pledged by major industrial groups. These funds align with apparel brands' ESG goals and aim to boost export capacity while enhancing sustainability in manufacturing. Highlighted by The Business Standard in a recent report.</p> */}
        <div
          className="h-[400px] max-w-6xl auto"
          dangerouslySetInnerHTML={{ __html: singleData?.value }}
        />
        <div className="h-[400px] max-w-6xl auto">{singleData?.value}</div>
        <div className="h-[400px] max-w-6xl auto my-6">
          {/* <Image src={img3} alt="" className="h-full w-full object-cover" /> */}
        </div>
        {/* <p className="pb-6 text-justify">Hamim Group, one of the largest apparel exporters, is investing $36 million into a new jacket manufacturing unit targeting high-end European brands like Hugo Boss, Ralph Lauren, and Tommy Hilfiger. The group is moving into jackets and outerwear to diversify from its primary offering of bottoms and woven garments. Hamim has also brought on an Indonesian partner to provide technical expertise in this new product segment.</p> */}

        {/* <Image src={img3} alt="" /> */}

        {/* <p className="py-6 text-justify">Windy Group is investing $36 million using self-financed funds to expand capacity. DBL Group, another leading exporter, is also making large investments in textiles.</p> */}
        <div className="h-[400px] max-w-6xl auto">
          {/* <Image src={img4} alt="" className="h-full w-full object-cover" /> */}
        </div>
        {/* <p className="my-6 text-justify">Denim producer Team Group is investing in two new knitwear and sweater factories, an accessories unit, and a green denim plant with zero discharge technology. Pacific Jeans is investing $35 million into an outerwear and formalwear factory in the Chittagong EPZ zone along with a recycling plant to meet sustainability targets.</p>
                <p className="my-6 text-justify">Nipa Group is investing $36 million in new spinning and dyeing mills for MMF-based products. Adzi Trims, part of Indet Group, is investing to double the capacity of their accessories production. They are also building an industrial park that would cover all types of accessories production in a single place. Sasha Denim is also investing $36 million in green garment and washing factories.</p>
                <p className="my-6 text-justify">The investments are being driven by expectations of steady growth and recovery in export demand. Groups investing now are aiming to boost capacities and capture larger market share. The focus on sustainability and green technologies aligns with global apparel brands' ESG priorities.</p>
                <p className="my-6 font-semibold text-black leading-6 text-justify">However, Bangladesh needs more investments in MMF production to reduce import reliance and meet expected global demand growth for synthetic textiles. The country also needs to address infrastructure issues like uninterrupted utilities and port operations to remain an attractive apparel export hub.</p> */}
        {/* <div className="my-14 grid grid-cols-3 gap-7">
          <div>
            <p>Source ---</p>
            <p className="mt-1.5">The Business Standard</p>
          </div>
          <div>
            <p>Photo Courtesy ---</p>
            <p className="mt-1.5">
              Hamim Group, Windy Group, DBL Group,
              <br />
              Team Group, pacific Jeans, Adzi Trims.
            </p>
          </div>
          <div className="flex flex-col items-end">
            <p className="ml-6">Share article ---</p>
            <div className="mt-1.5 flex justify-satrt items-center gap-2.5 ml-1 text-2xl text-[#4E9345]">
              <FaFacebookSquare />
              <FaTwitterSquare />
              <FaLinkedin />
              <FaWhatsappSquare />
            </div>
          </div>
        </div> */}

        {/* <p className="text-black font-semibold text-center">
          More related article
        </p> */}
      </div>
      <div className="max-w-7xl mx-auto grid grid-cols-3 gap-8 pb-12 pt-5">
        <div>
          {/* <Image src={img1} alt="" /> */}
          {/* <div className="mt-3 px-1">
            Top Ten Green Germents Manufacturers with lead certifications in
            Bangladesh
          </div> */}
        </div>
        <div>
          {/* <Image src={img1} alt="" /> */}
          {/* <div className="mt-3 px-1">1.1 billion fresh investment for ESG</div> */}
        </div>
        <div>
          {/* <Image src={img1} alt="" /> */}
          <div className="mt-3 px-1">
            {/* Meet Bayla - naxt generation of Bangladesh Apparel Industry{" "} */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContentsDetails;
