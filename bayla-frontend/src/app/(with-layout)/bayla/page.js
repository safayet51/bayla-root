"use client";

import BaylaBoard from "@/components/UI/BaylaPageUI/BaylaBoard";
import BaylaPageUI from "@/components/UI/BaylaPageUI/BaylaPageUI";
import ContactForm from "@/components/UI/BaylaPageUI/ContactForm";
import PresicentMeet from "@/components/UI/BaylaPageUI/PresicentMeet";

const BaylaPage = () => {
  return (
    <>
      <BaylaPageUI />
      <PresicentMeet />
      <BaylaBoard />
      <ContactForm />
    </>
  );
};

export default BaylaPage;
