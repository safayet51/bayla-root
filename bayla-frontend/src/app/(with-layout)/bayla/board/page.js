"use client";

import Link from "next/link";
import img1 from "../../../../assets/baylaMember/president_image.png";
import img2 from "../../../../assets/baylaMember/al_shahriar_ahmed.png";
import img3 from "../../../../assets/baylaMember/hasin_arman.png";
import img4 from "../../../../assets/baylaMember/sakib_ahmed.png";
import img5 from "../../../../assets/baylaMember/rafee_mahmood.png";
import img6 from "../../../../assets/baylaMember/mk_alam.png";
import img7 from "../../../../assets/baylaMember/aqib_jafri_sharif.png";
import img8 from "../../../../assets/baylaMember/md_jasim_uddin.png";
import img9 from "../../../../assets/baylaMember/ehsan_haq.png";
import img10 from "../../../../assets/baylaMember/azfar_hassan.png";
import img11 from "../../../../assets/baylaMember/zarin_rashid.png";
import img12 from "../../../../assets/baylaMember/abrar_alam_khan.png";
import img13 from "../../../../assets/baylaMember/ramize_khalid_islam.png";
import img14 from "../../../../assets/baylaMember/lithe_moontaha_mohiuddin.png";
import img15 from "../../../../assets/baylaMember/subael_sarwar.png";
import img16 from "../../../../assets/baylaMember/kazi_fahad.png";
import CardWithAverter from "@/components/shared/CardWithAverter";
import { BsArrowLeftShort } from "react-icons/bs";

const page = () => {
  const boardData = [
    {
      id: "1",
      name: "Abrar Hossain Sayem",
      email: "president@bayla.org",
      designation: "President",
      img: img1,
    },
    {
      id: "2",
      name: "Al-Shahriar Ahmed",
      email: "secretary@bayla.org",
      designation: "Secretary",
      img: img2,
    },
    {
      id: "3",
      name: "Hasin Arman",
      email: "treasurer@bayla.org",
      designation: "Treasurer",
      img: img3,
    },
    {
      id: "4",
      name: "Sakib Ahmed",
      email: "svp.sakib@bayla.org",
      designation: "Senior Vice President",
      img: img4,
    },
    {
      id: "5",
      name: "Rafee Mahmood",
      email: "vp.rafee@bayla.org",
      designation: "Vice President",
      img: img5,
    },
    {
      id: "6",
      name: "MK Alam",
      email: "vp.alam@bayla.org",
      designation: "Vice President",
      img: img6,
    },
    {
      id: "7",
      name: "Aqib Jafri Sharif",
      email: "additional.secretary@bayla.org",
      designation: "Additional Secretary",
      img: img7,
    },
    {
      id: "8",
      name: "Md. Jasim Uddin",
      email: "jasim@bayla.org",
      designation: "Director",
      img: img8,
    },
    {
      id: "9",
      name: "Ehsan haq",
      email: "ehsaan@bayla.org",
      designation: "Director",
      img: img9,
    },
    {
      id: "10",
      name: "Azfar Hassan",
      email: "azfar@bayla.org",
      designation: "Director",
      img: img10,
    },
    {
      id: "11",
      name: "Zarin Rashid",
      email: "zarin@bayla.org",
      designation: "Director",
      img: img11,
    },
    {
      id: "12",
      name: "Abrar Alam Khan",
      email: "abrar@bayla.org",
      designation: "Director",
      img: img12,
    },
    {
      id: "13",
      name: "Ramize Khalid Islam",
      email: "ramize@bayla.org",
      designation: "Director",
      img: img13,
    },
    {
      id: "14",
      name: "Lithe Moontaha Mohiuddin",
      email: "",
      designation: "Director",
      img: img14,
    },
    {
      id: "15",
      name: "Subael Sarwar",
      email: "subael@bayla.com",
      designation: "Director",
      img: img15,
    },
    {
      id: "16",
      name: "Kazi Fahad",
      email: "kazi@bayla.org",
      designation: "Director",
      img: img16,
    },
  ];
  return (
    <div className="max-w-8xl">
      <div className="relative">
        <Link
          href="/bayla"
          className="text-gray-500 font-medium absolute left-[300px] top-2 cursor-pointer">
          <div className="flex items-center gap-1">
            <BsArrowLeftShort className="text-2xl" />
            <p>Go back</p>
          </div>
        </Link>
        <h2 className="font-bold text-center my-24 text-3xl">
          Member Factories
        </h2>
      </div>
      <div className="grid grid-cols-4 gap-5 max-w-7xl mx-auto mb-8">
        {boardData?.map((e, i) => {
          return <CardWithAverter key={i} e={e} />;
        })}
      </div>
    </div>
  );
};

export default page;
