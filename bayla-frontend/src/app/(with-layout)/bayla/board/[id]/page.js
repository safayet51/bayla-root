import Image from "next/image";
import Link from "next/link";
import { BsArrowLeftShort } from "react-icons/bs";

const page = () => {
  return (
    <div>
      <div className="relative">
        <Link
          href="/bayla"
          className="text-gray-500 font-medium absolute left-[300px] top-2 cursor-pointer">
          <div className="flex items-center gap-1">
            <BsArrowLeftShort className="text-2xl" />
            <p>Go back</p>
          </div>
        </Link>
      </div>
      <div className="flex flex-col max-w-6xl mx-auto justify-center items-center mt-10">
        {/* <Image src={img1} alt="" /> */}
        <div>
          <h2 className="text-2xl font-bold">Abrar Hossain Sayem</h2>
          <p className="text-center">President, BAYLA</p>
          <div className="flex justify-center mt-3 items-center gap-3">
            {/* <Image src={img3} alt="" />
            <Image src={img2} alt="" /> */}
          </div>
        </div>
        <div className="border rounded-lg p-16 my-10">
          <p className="font-bold text-bold text-xl">Background</p>
          <p className="text-justify text-xl ">
            To face the challenges and grab the opportunities of the fourth
            industrial revolution young leaders of the industry came to mutual
            consent on jointly increasing their effort and thus forming a
            youth-based association called BAYLA (Bangladesh Apparel Youth
            Leaders Association). This association will work closely with
            leading associations of the industry like BGMEA, BKMEA, BTMA and
            BGAPMEA. This unique proposal of this body is to research
            innovation, bring youth leaders together, finding new opportunities
            for training, capacity building, and market expansion. The team is
            expected to be contributing significantly to the apparel industry of
            Bangladesh. This is solely a non-political association born out of
            the motivation and determination of youth. Any leader running an
            organization related to apparel manufacturing and is aged below 50
            can be a part of the association.
          </p>
          <p className="font-bold text-bold text-xl mt-10">Education</p>
          <p className="text-justify text-xl ">
            To face the challenges and grab the opportunities of the fourth
            industrial revolution young leaders of the industry came to mutual
            consent on jointly increasing their effort and thus forming a
            youth-based association called BAYLA (Bangladesh Apparel Youth
            Leaders Association). This is solely a non-political association
            born out of the.
          </p>
          <p className="font-bold text-bold text-xl mt-10">Mission</p>
          <p className="text-justify text-xl ">
            To face the challenges and grab the opportunities of the fourth
            industrial revolution young leaders of the industry came to mutual
            consent on jointly increasing their effort and thus forming a
            youth-based association called BAYLA (Bangladesh Apparel Youth
            Leaders Association). This association will work closely with
            leading associations of the industry like BGMEA, BKMEA, BTMA and
            BGAPMEA. This unique proposal of this body is to research
            innovation, bring youth leaders together, finding new opportunities
            for training, capacity building, and market expansion. The team is
            expected to be contributing significantly to the apparel industry of
            Bangladesh.
          </p>
          <p className="font-bold text-bold text-xl mt-10">Vission</p>
          <p className="text-justify text-xl ">
            To face the challenges and grab the opportunities of the fourth
            industrial revolution young leaders of the industry came to mutual
            consent on jointly increasing their effort and thus forming a
            youth-based association called BAYLA (Bangladesh Apparel Youth
            Leaders Association). This association will work closely with
            leading associations of the industry like BGMEA, BKMEA, BTMA and
            BGAPMEA. This unique proposal of this body is to research
            innovation, bring youth leaders together, finding new opportunities
            for training, capacity building, and market expansion. The team is
            expected to be contributing significantly to the apparel industry of
            Bangladesh.
          </p>
        </div>
      </div>
    </div>
  );
};

export default page;
