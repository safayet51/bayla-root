"use client";

import React, { useEffect, useState } from "react";
import { publicationList } from "../../../../components/shared/data";
import Iframe from "react-iframe";

const page = ({ params: { id } }) => {
  const [pdfInfo, setPdfInfo] = useState(null);

  useEffect(() => {
    const foundPdf = publicationList.find((e) => e.id == id);
    setPdfInfo(foundPdf);
  }, [id]);
  return (
    <div>
      {pdfInfo?.id == 1 ? (
        <Iframe
          url="/magazine4.pdf"
          width="100%"
          height="1000px"
          id=""
          className=""
          display="block"
          position="relative"
        />
      ) : pdfInfo?.id == 2 ? (
        <Iframe
          url="/magazine2.pdf"
          width="100%"
          height="1000px"
          id=""
          className=""
          display="block"
          position="relative"
        />
      ) : pdfInfo?.id == 4 ? (
        <Iframe
          url="/magazine1.pdf"
          width="100%"
          height="1000px"
          id=""
          className=""
          display="block"
          position="relative"
        />
      ) : pdfInfo?.id == 3 ? (
        <Iframe
          url="/magazine3.pdf"
          width="100%"
          height="1000px"
          id=""
          className=""
          display="block"
          position="relative"
        />
      ) : (
        ""
      )}
    </div>
  );
};

export default page;
