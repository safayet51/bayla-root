import Navbar from "@/components/shared/Navbar";

function WithLayout({ children }) {
  return (
    <div>
      <Navbar />
      {children}
    </div>
  );
}

export default WithLayout;
