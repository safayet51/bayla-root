
import PerkLeft from "@/components/UI/PerksPageUi/PerkLeft"
import img1 from "../../../assets/perks/1.png"
import img2 from "../../../assets/perks/2.png"
import img3 from "../../../assets/perks/3.png"
import img4 from "../../../assets/perks/4.png"
import img5 from "../../../assets/perks/5.png"
import img6 from "../../../assets/perks/6.png"
import PerksCard from "@/components/UI/PerksPageUi/PerksCard"

const PerksPage = () => {
    const perksContents = [
        {
            id: "1",
            image: img1,
            percent: "25%",
            name: "ZAF Germents",
            date: "20-12-24",
            week: "10 week left"
        },
        {
            id: "2",
            image: img2,
            percent: "25%",
            name: "Domino's Pizza",
            date: "20-12-24",
            week: "10 week left"
        },
        {
            id: "3",
            image: img3,
            percent: "40%",
            name: "Tokko Fashions",
            date: "24-12-23",
            week: "10 week left"
        },
        {
            id: "4",
            image: img4,
            percent: "30%",
            name: "Nazimgrah Resorts",
            date: "14-12-23",
            week: "19 week left"
        },
        {
            id: "5",
            image: img5,
            percent: "60%",
            name: "Being Human",
            date: "12-12-23",
            week: "18 week left"
        },
        {
            id: "6",
            image: img6,
            percent: "45%",
            name: "US Polo Assn.",
            date: "20-02-24",
            week: "23 week left"
        },
    ]
    return (
        <div className="grid grid-cols-3 gap-0 max-w-8xl my-10 mx-auto">

            <div>
                <PerkLeft />
            </div>
            <div className="col-span-2 mr-28">

                <p className="text-black text-2xl font-semibold ml-7">BAYLA Privilege Partners</p>
                <div className="grid grid-cols-2 gap-5 items-stretch">
                    {
                        perksContents?.map((e, i) => <PerksCard e={e} key={i} />)
                    }
                </div>

            </div>
        </div>
    )
}

export default PerksPage