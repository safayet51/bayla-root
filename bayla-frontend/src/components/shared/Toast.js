"use client";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import React from "react";

const Toast = () => {
  return (
    <div>
      {" "}
      <ToastContainer />
    </div>
  );
};

export default Toast;
