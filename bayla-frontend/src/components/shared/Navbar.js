"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import { MdOutlineMenu } from "react-icons/md";
import { menus } from "./data";
import { useState } from "react";
import { motion } from "framer-motion";
const Navbar = () => {
  const pathname = usePathname();
  const [open, setOpen] = useState(false);
  const toggleOpen = () => {
    setOpen(!open);
  };
  return (
    <div
      className="navbar bg-transparent justify-start lg:justify-center md:justify-start"
      style={{
        zIndex: "999",
        position: "relative",
      }}>
      {/* humberger menu for small device */}
      <div className="dropdown">
        <label
          tabIndex={0}
          className="btn btn-ghost lg:hidden"
          onClick={toggleOpen}>
          <MdOutlineMenu className="text-xl" />
        </label>
        {open && (
          <ul
            tabIndex={0}
            className={`bg-white text-[14px] menu-sm dropdown-content mt-[2px] px-2 shadow rounded-md w-80 z-10 py-5`}
            style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px" }}>
            {menus?.map((e, i) => {
              return (
                <li className="mt-4">
                  <Link
                    key={i}
                    href={e.link}
                    className={
                      pathname == e.link
                        ? "text-[#2C9244] text-[19px] font-semibold"
                        : "hover:text-[#2C9244] text-[19px] font-semibold"
                    }>
                    {e?.value}
                  </Link>
                </li>
              );
            })}
          </ul>
        )}
      </div>

      {/* navbar for large and medium device */}
      <div className="navbar-center hidden lg:flex">
        <motion.ul
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.4, delay: 0.3 }}
          className={`menu-horizontal px-1 gap-16`}>
          {menus?.map((e, i) => {
            return (
              <Link
                key={i}
                href={e.link}
                className={
                  pathname == e.link
                    ? "text-[#2C9244] text-[19px] font-bold pt-2"
                    : "hover:text-[#2C9244] text-[19px] font-semibold pt-2" &&
                      pathname == "/youth-conference"
                    ? "text-white text-[19px] font-semibold pt-2"
                    : "hover:text-[#2C9244] text-[19px] font-semibold pt-2"
                }>
                {e?.value}
              </Link>
            );
          })}
        </motion.ul>
      </div>
    </div>
  );
};

export default Navbar;
