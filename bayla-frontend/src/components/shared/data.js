import publicationImage1 from "../../assets/publications/publication1.png";
import publicationImage2 from "../../assets/publications/publication2.png";
import publicationImage3 from "../../assets/publications/publication3.png";
import publicationImage4 from "../../assets/publications/publication4.png";

import img1 from "../../assets/baylaMember/president_image.png";
import img2 from "../../assets/baylaMember/al_shahriar_ahmed.png";
import img3 from "../../assets/baylaMember/hasin_arman.png";
import img4 from "../../assets/baylaMember/sakib_ahmed.png";
import img5 from "../../assets/baylaMember/rafee_mahmood.png";
import img6 from "../../assets/baylaMember/mk_alam.png";
import img7 from "../../assets/baylaMember/aqib_jafri_sharif.png";
import img8 from "../../assets/baylaMember/md_jasim_uddin.png";
import img9 from "../../assets/baylaMember/ehsan_haq.png";
import img10 from "../../assets/baylaMember/azfar_hassan.png";
import img11 from "../../assets/baylaMember/zarin_rashid.png";
import img12 from "../../assets/baylaMember/abrar_alam_khan.png";
import img13 from "../../assets/baylaMember/ramize_khalid_islam.png";
import img14 from "../../assets/baylaMember/lithe_moontaha_mohiuddin.png";
import img15 from "../../assets/baylaMember/subael_sarwar.png";
import img16 from "../../assets/baylaMember/kazi_fahad.png";
export const menus = [
  { link: "/", value: "#BRANDINGBANGLADESH" },
  { link: "/contents", value: "Contents" },
  { link: "/publications", value: "Publications" },
  { link: "/bayla", value: "BAYLA" },
  { link: "/youth-conference", value: " Youth Conference" },
];

export const publicationList = [
  {
    id: 4,
    image: publicationImage4,
    tag: "BAYLA INSIGHT",
    title: "Sustain and Enable",
    edition: "Fourth Edition",
    date: "November 2022",
  },
  {
    id: 3,
    image: publicationImage3,
    tag: "BAYLA INSIGHT",
    title: "Road to Recovery",
    edition: "Third Edition",
    date: "March 2022",
  },
  {
    id: 2,
    image: publicationImage2,
    tag: "BAYLA INSIGHT",
    title: "SWOT Analysis of Bangladesh RMG Ltd.",
    edition: "Second Edition",
    date: "July 2021",
  },
  {
    id: 1,
    image: publicationImage1,
    tag: "BAYLA INSIGHT",
    title: "Branding Bangladesh",
    edition: "First Edition",
    date: "March 2021",
  },
];

export const boardData = [
  {
    id: "1",
    name: "Abrar Hossain Sayem",
    email: "president@bayla.org",
    designation: "President",
    linkedIn: "https://www.linkedin.com/in/abrar-sayem-50b02855/",
    img: img1,
  },
  {
    id: "2",
    name: "Al-Shahriar Ahmed",
    email: "secretary@bayla.org",
    designation: "Secretary",
    linkedIn: null,
    img: img2,
  },
  {
    id: "3",
    name: "Hasin Arman",
    linkedIn: "https://www.linkedin.com/in/hasin-arman/",
    email: "treasurer@bayla.org",
    designation: "Treasurer",
    img: img3,
  },
  {
    id: "4",
    name: "Sakib Ahmed",
    email: "svp.sakib@bayla.org",
    linkedIn: "https://www.linkedin.com/in/sakib-ahmed-95684213/",
    designation: "Senior Vice President",
    img: img4,
  },
  {
    id: "5",
    name: "Rafee Mahmood",
    linkedIn: "https://www.linkedin.com/in/rafeemahmood/",
    email: "vp.rafee@bayla.org",
    designation: "Vice President",
    img: img5,
  },
  {
    id: "6",
    name: "MK Alam",
    linkedIn: "https://www.linkedin.com/in/mk-alam-a2pgroup/",
    email: "vp.alam@bayla.org",
    designation: "Vice President",
    img: img6,
  },
  {
    id: "7",
    name: "Aqib Jafri Sharif",
    linkedIn: "https://www.linkedin.com/in/aqib-jafri-sharif-751567121/",
    email: "additional.secretary@bayla.org",
    designation: "Additional Secretary",
    img: img7,
  },
  {
    id: "8",
    name: "Md. Jasim Uddin",
    linkedIn: "https://www.linkedin.com/in/jasim2223/",
    email: "jasim@bayla.org",
    designation: "Director",
    img: img8,
  },
  {
    id: "9",
    name: "Ehsan haq",
    email: "ehsaan@bayla.org",
    designation: "Director",
    linkedIn: "https://www.linkedin.com/in/ehsan-haq-083b787a/",
    img: img9,
  },
  {
    id: "10",
    name: "Azfar Hassan",
    email: "azfar@bayla.org",
    linkedIn: "https://www.linkedin.com/in/azfar-hassan-8a738b117/",
    designation: "Director",
    img: img10,
  },
  {
    id: "11",
    name: "Zarin Rashid",
    linkedIn: "https://www.linkedin.com/in/zarin-rashid-521866271/",
    email: "zarin@bayla.org",
    designation: "Director",
    img: img11,
  },
  {
    id: "12",
    name: "Abrar Alam Khan",
    email: "abrar@bayla.org",
    designation: "Director",
    linkedIn: "https://www.linkedin.com/in/abrar-khan-15423a125/",
    img: img12,
  },
  {
    id: "13",
    name: "Ramize Khalid Islam",
    linkedIn: "https://www.linkedin.com/in/ramiz-khalid-islam-49168b127/",
    email: "ramize@bayla.org",
    designation: "Director",
    img: img13,
  },
  {
    id: "14",
    name: "Lithe Moontaha Mohiuddin",
    linkedIn: "https://www.linkedin.com/in/lithe05/",
    email: "",
    designation: "Director",
    img: img14,
  },
  {
    id: "15",
    name: "Subael Sarwar",
    linkedIn: "https://www.linkedin.com/in/subael-sarwar-99b91b77/",
    email: "subael@bayla.com",
    designation: "Director",
    img: img15,
  },
  {
    id: "16",
    name: "Kazi Fahad",
    linkedIn: "https://www.linkedin.com/in/kazi-fahad-a95a5464/",
    email: "kazi@bayla.org",
    designation: "Director",
    img: img16,
  },
];
