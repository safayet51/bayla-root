"use client"
import Image from "next/image"
import img1 from "../../assets/bayla/1.png"
import img2 from "../../assets/bayla/2.png"
import img3 from "../../assets/bayla/3.png"
import img4 from "../../assets/bayla/4.png"
import img5 from "../../assets/bayla/5.png"
import img6 from "../../assets/bayla/6.png"
import img7 from "../../assets/bayla/7.png"
import img8 from "../../assets/bayla/8.png"
import { Dialog } from "primereact/dialog";
const DialogModal = ({ visible, setVisible }) => {
    const memberData = [
        {
            id: "1",
            name: "Asortex Group",
            location: "Dhaka",
            img: img1
        },
        {
            id: "2",
            name: "Ananta Companies",
            location: "Uttara, Dhaka",
            img: img2
        },
        {
            id: "3",
            name: "Sayem Group",
            location: "Uttara, Dhaka",
            img: img3
        },
        {
            id: "4",
            name: "Anowara Group",
            location: "Narayangonj",
            img: img4
        },
        {
            id: "5",
            name: "Adzi Trims Ltd.",
            location: "Uttara, Dhaka",
            img: img5
        },
        {
            id: "6",
            name: "Assurance Moni Group",
            location: "Banani, Dhaka",
            img: img6
        },
        {
            id: "7",
            name: "Mohmud Group",
            location: "Uttara, Dhaka",
            img: img7
        },
        {
            id: "8",
            name: "Northern Tosfia Group",
            location: "Tongi, Gazipur",
            img: img8
        }
    ]
    return (
        <Dialog header="Member Factories" visible={visible} onHide={() => setVisible(false)}
            breakpoints={{ '960px': '75vw', '641px': '100vw' }} style={{ width: '50vw', boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px", background: "white", borderBottom: "2px solid gray" }} >
            <div className="grid grid-cols-4 gap-5 justify-items-center">
                {
                    memberData?.map((e, i) => <div key={i} className="text-center mt-6">
                        <Image src={e?.img} alt="" />
                        <p className="font-bold">{e?.name}</p>
                        <p>{e?.location}</p>
                    </div>)
                }

            </div>
        </Dialog>
    )
}

export default DialogModal