"use client";

import Sidebar, { SidebarItem } from "./SideBar";
import { PiNotebookBold } from "react-icons/pi";
import { GoChecklist } from "react-icons/go";
import { MdContacts } from "react-icons/md";
import { FiLogOut } from "react-icons/fi";
import { signOut } from "next-auth/react";

const Side = () => {
  return (
    <Sidebar>
      <SidebarItem
        icon={<PiNotebookBold size={20} />}
        title="Contents"
        path="/admin/content"
      />
      <SidebarItem
        icon={<GoChecklist size={20} />}
        title="Promote"
        path="/admin/promote"
      />
      <SidebarItem
        icon={<MdContacts size={20} />}
        title="Contact"
        path="/admin/contact"
      />

      <SidebarItem
        onClick={() => {
          signOut();
        }}
        icon={<FiLogOut size={20} />}
        title="logout"
        path="/admin"
      />
    </Sidebar>
  );
};

export default Side;
