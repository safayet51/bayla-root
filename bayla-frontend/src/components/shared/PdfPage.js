"use client"

import Iframe from "react-iframe"

const PdfPage = () => {
    return (
        <div>
            <Iframe url="https://bayla.org/img/BAYLA_Magazine_issue_4.pdf"
                width="100%"
                height="100%"
                id=""
                className=""
                display="block"
                position="relative" />

        </div>
    )
}

export default PdfPage