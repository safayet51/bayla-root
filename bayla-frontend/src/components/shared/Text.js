

const Text = ({ title, color }) => {
    return (
        <h2 className="font-bold text-3xl" style={{ color: `${color}` }}>{title}</h2>
    )
}

export default Text