"use client"
const Button = ({ title, bgColor, paddingX, paddingY, color, radius, borderColor, width, fontWeight, fontSize }) => {
    return (
        <button style={{ background: `${bgColor}`, padding: `${paddingY}px ${paddingX}px`, color: `${color}`, border: `1px solid ${borderColor}`, borderRadius: `${radius}px`, width: `${width}%`, fontWeight: `${fontWeight}`, fontSize: `${fontSize}px` }}>{title}</button>
    )
}


export default Button;