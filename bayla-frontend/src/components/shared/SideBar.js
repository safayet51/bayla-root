"use client";

import { MdMenu, MdOutlineMenuOpen } from "react-icons/md";
import { useContext, createContext, useState } from "react";
import NavLink from "./NavLink";
import Link from "next/link";

const SidebarContext = createContext();

const Sidebar = ({ children }) => {
  const [expanded, setExpanded] = useState(true);
  return (
    <aside
      className="h-screen"
      style={{
        boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
      }}>
      <nav className="h-full flex flex-col bg-white border-r shadow-sm">
        <div className="px-2 pb-2 flex justify-between items-center mt-3">
          <Link href="/">
            {" "}
            <p
              className={`overflow-hidden transition-all font-semibold text-xl ${
                expanded ? "w-32" : "w-0"
              }`}>
              BaylaAdmin
            </p>
          </Link>
          <button
            onClick={() => setExpanded((curr) => !curr)}
            className="font-medium rounded-md cursor-pointer hover:bg-gray-100">
            {expanded ? (
              <MdMenu className="font-medium text-xl" />
            ) : (
              <MdOutlineMenuOpen className="font-medium text-xl" />
            )}
          </button>
        </div>

        <SidebarContext.Provider value={{ expanded }}>
          {children}
        </SidebarContext.Provider>
      </nav>
    </aside>
  );
};
export default Sidebar;

export function SidebarItem({ icon, path, active, title, func }) {
  const { expanded } = useContext(SidebarContext);

  return (
    <NavLink
      href={`${path}`}
      exact
      activeClassName="bg-[#2c9244] text-white"
      className={`
            relative flex items-center py-2 px-4 my-1
            font-medium rounded-md cursor-pointer
            transition-colors group mx-2
        `}>
      {icon}
      <span
        className={`overflow-hidden transition-all ${
          expanded ? "w-52 ml-3" : "w-0"
        }`}>
        {title}
      </span>

      {!expanded && (
        <div
          className={`
          absolute left-full rounded-[4px] px-2 py-1 ml-6
          bg-[#2c9244] text-white text-sm
          invisible opacity-20 -translate-x-3 transition-all
          group-hover:visible group-hover:opacity-100 group-hover:translate-x-0
      `}>
          {title}
        </div>
      )}
    </NavLink>
  );
}
