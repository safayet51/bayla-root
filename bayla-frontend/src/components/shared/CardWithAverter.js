"use client";

import Image from "next/image";
import { FaLinkedin } from "react-icons/fa";

const CardWithAverter = ({ e }) => {
  return (
    <div className="hover:shadow-lg py-10">
      <div className="avatar flex justify-center item-center">
        <div className="w-32 rounded-full bg-gray-500 ">
          <Image src={e?.img} alt="" />
        </div>
      </div>
      <p className="text-xl font-medium mt-4 text-center">{e?.name}</p>
      <p>{e?.location}</p>
      <p className="text-center my-2">{e?.designation}</p>
      <p className="text-center">{e?.email}</p>

      <div className="flex justify-center items-center">
        {e?.linkedIn ? (
          <a href={e?.linkedIn}>
            <FaLinkedin className="text-3xl text-blue-600" />
          </a>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default CardWithAverter;
