"use client"

import Button from "@/components/shared/Button"
import Text from "@/components/shared/Text"
import { BiCurrentLocation, BiSolidStopwatch, BiStopwatch } from "react-icons/bi"
import Image from "next/image"

const ScheduleCard = ({ e }) => {
    return (
        <div className="grid grid-cols-2 gap-5 max-w-6xl mx-auto border rounded-xl mt-9 mb-10">
            {
                e?.id % 2 !== 0 ? <>  <div className="w-[576px]">
                    <Image src={e?.img} alt="image" className="h-full w-full object-cover rounded-s-xl" />
                </div>
                    <div className="text-left p-9 space-y-5">
                        <Button title={e.day} bgColor={"#fff"} borderColor={"#10A9C1"} color={"#10A9C1"} radius={10} width={80} paddingX={13} paddingY={4} />
                        <Text title={e?.name} />
                        <div className="flex justify-normal items-center gap-5"> <BiCurrentLocation className="text-3xl text-[#10A9C1]" /><p>{e.location}</p></div>
                        <div className="flex justify-normal items-center gap-5"> <BiStopwatch className="text-3xl text-[#10A9C1]" /><p>{e.date}</p></div>
                        <Button title={e.newText} bgColor={"#10A9C1"} borderColor={"#10A9C1"} color={"#fff"} radius={10} width={80} paddingX={13} paddingY={4} />
                    </div></> : <>
                    <div className="text-left p-9 space-y-5">
                        <Button title={e.day} bgColor={"#fff"} borderColor={"#10A9C1"} color={"#10A9C1"} radius={10} width={80} paddingX={13} paddingY={4} />
                        <Text title={e?.name} />
                        <div className="flex justify-normal items-center gap-5"> <BiCurrentLocation className="text-3xl text-[#10A9C1]" /><p>{e.location}</p></div>
                        <div className="flex justify-normal items-center gap-5"> <BiStopwatch className="text-3xl text-[#10A9C1]" /><p>{e.date}</p></div>
                        <Button title={e.newText} bgColor={"#10A9C1"} borderColor={"#10A9C1"} color={"#fff"} radius={10} width={80} paddingX={13} paddingY={4} />
                    </div>
                    <div className="w-[576px]">
                        <Image src={e?.img} alt="image" className="h-full w-full object-cover rounded-e-xl" />
                    </div></>
            }

        </div>
    )
}

export default ScheduleCard