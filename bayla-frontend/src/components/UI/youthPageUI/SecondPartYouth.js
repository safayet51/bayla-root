"use client";
import Image from "next/image";
import img1 from "../../../assets/youth/Inspiration & Empowerment Icon.png";
import img2 from "../../../assets/youth/Knowledge Sharing Icon.png";
import img3 from "../../../assets/youth/Networking Icon.png";
import Text from "@/components/shared/Text";

const SecondPartYouth = () => {
  const cardData = [
    {
      id: "1",
      img: img3,
      name: "Networking",
      msg: "Youth leaders recognize the power of networking in expanding their horizons, forging valuable partnerships, and creating opportunities for collaboration within the industry.",
    },
    {
      id: "2",
      img: img2,
      name: "Knowledge Sharing",
      msg: "The conference provides a unique platform for youth leaders to exchange ideas, strategies, and experiences.It's a space where knowledge flows freely, fostering innovation and growth.",
    },
    {
      id: "3",
      img: img1,
      name: "Inspiration & Empowerment",
      msg: "Attending the conference isnt just about gaining knowledge; it's sabout being inspired by industry trailblazers and gaining the confidence to make a meaningful impact in the world of apparel.",
    },
  ];
  return (
    <div className="backdrop-blur-sm bg-[#10A9C1]/70">
      <div className="grid grid-cols-2 gap-10 justify-start items-start max-w-[1300px] mx-auto text-white pt-12">
        <div className="mt-10 leading-relaxed">
          <h2
            className="text-5xl font-black leading-relaxed"
            style={{ textShadow: "0px 4px 9px #6d6d6d" }}>
            What is BAYLA
          </h2>
          <h2
            className="text-5xl font-black leading-relaxed"
            style={{ textShadow: "0px 4px 9px #6d6d6d" }}>
            Youth Conference
          </h2>
        </div>
        <p className="text-xl mt-10 text-justify leading-relaxed font-medium">
          The BAYLA Youth Conference is a dynamic gathering of young leaders in
          the apparel industry, focusing on Knowledge, Innovation, Research, and
          Inclusion. This platform fosters collaborative growth and empowers
          youth to shape the future of Bangladesh's apparel sector. Join us for
          an inspiring exploration of industry insights and opportunities.
        </p>
        <div className="mt-16 leading-relaxed">
          <h2
            className="text-5xl font-black leading-relaxed"
            style={{ textShadow: "0px 4px 9px #6d6d6d" }}>
            Why Youths are
          </h2>
          <h2
            className="text-5xl font-black leading-relaxed"
            style={{ textShadow: "0px 4px 9px #6d6d6d" }}>
            Coming together?
          </h2>
        </div>

        <p className="text-xl mt-16 text-justify leading-relaxed font-medium">
          Youth eagerly join the BAYLA Youth Conference to gain invaluable
          industry insights, connect with fellow trailblazers, and harness the
          transformative power of knowledge and innovation. It's a platform
          where their voices are heard, ideas celebrated, and where they can
          make a meaningful impact in shaping the future of the apparel sector.
        </p>
      </div>
      <div className="grid grid-cols-3 gap-5 mt-28 justify-items-center max-w-7xl mx-auto pb-18 py-20">
        {cardData?.map((e, i) => (
          <div
            key={i}
            className="card w-96 border border-white text-white"
            style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px" }}>
            <div className="card-body">
              <div className="avatar">
                <div className="w-24 rounded-full mx-auto">
                  <Image src={e?.img} alt="" />
                </div>
              </div>
              <h2 className="mt-3 font-bold text-2xl text-center">{e?.name}</h2>
              <p className="text-center text-xl leading-relaxed font-medium">
                {e?.msg}
              </p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default SecondPartYouth;
