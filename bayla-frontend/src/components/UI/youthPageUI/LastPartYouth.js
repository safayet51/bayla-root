"use client"
import img1 from "../../../assets/youth/Schedule 1.png"
import img2 from "../../../assets/youth/Schedule 2.png"
import img3 from "../../../assets/youth/Schedule 3.png"
import img4 from "../../../assets/youth/Schedule 4.png"
import Text from "@/components/shared/Text"
import ScheduleCard from "./ScheduleCard"

const LastPartYouth = () => {
    const cardContent2 = [
        {
            id: "1",
            name: "Press Conference",
            location: "Intercontinental Hotel, Dhaka",
            date: "3rd December, 2023  |   12:30 pm BST",
            img: img1,
            newText: "Entry Process: Invite Only",
            day: "Day 01"
        },
        {
            id: "2",
            name: "Inauguration by Honorable Prime Minister",
            location: "Bangabandhu International Conference Center (BICC)",
            date: "4th December, 2023  |   9:30 am-2:40 pm BST",
            img: img2,
            newText: "Entry Process: Invite Only",
            day: "Day 02"
        },
        {
            id: "3",
            name: "Youth Conference",
            location: "Hall - 4, ICCB",
            date: "5th December, 2023  |   11:00 am-8:00 pm BST",
            img: img3,
            newText: "Entry Process: Registration",
            day: "Day 03"
        },
        {
            id: "4",
            name: "Sustainable Design and Innovation Award",
            img: img4,
            location: "Hall - 2, ICCB",
            date: "6th December, 2023  |   6:00 pm-10:30 pm BST",
            newText: "Entry Process: Invite Only",
            day: "Day 04"
        }
    ]
    return (
        <div className="mt-24 text-center max-w-7xl mx-auto">
            <Text title={"Conference Schedule"} color="black" />
            <p className="text-xl">3 - 6 December, 2023</p>

            <div >
                {
                    cardContent2?.map((e, i) => <ScheduleCard key={i} e={e} />)
                }
            </div>
        </div>
    )
}

export default LastPartYouth