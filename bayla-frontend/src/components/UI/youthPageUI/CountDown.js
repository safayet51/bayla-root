"use client";
import { useEffect, useState } from "react";

const CountDown = () => {
  const targetDate = new Date("2024-03-01T00:00:00Z").getTime();
  const [time, setTime] = useState(calculateTimeRemaining());

  function calculateTimeRemaining() {
    const now = new Date().getTime();
    const difference = targetDate - now;

    if (difference <= 0) {
      return {
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
      };
    }

    const seconds = Math.floor(difference / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);
    const days = Math.floor(hours / 24);

    return {
      days: days,
      hours: hours % 24,
      minutes: minutes % 60,
      seconds: seconds % 60,
    };
  }

  useEffect(() => {
    const countdownInterval = setInterval(() => {
      setTime(calculateTimeRemaining());
    }, 1000);

    return () => clearInterval(countdownInterval);
  }, []);

  const getFormattedTime = ({ days, hours, minutes, seconds }) => {
    return (
      <div className="grid grid-flow-col gap-5 text-center auto-cols-max  justify-center justify-items-center mb-14">
        <div className="font-semibold">
          <div className="flex flex-col p-3 py-5 bg-gray-900 rounded-lg text-neutral-content mb-1">
            <span className="countdown font-mono text-5xl">
              <span style={{ "--value": `${days}` }}></span>
            </span>
          </div>
          Days
        </div>
        <div className="font-semibold">
          <div className="flex flex-col p-3 py-5 bg-gray-900 rounded-lg text-neutral-content mb-1">
            <span className="countdown font-mono text-5xl">
              <span style={{ "--value": `${hours}` }}></span>
            </span>
          </div>
          Hours
        </div>
        <div className="font-semibold">
          <div className="flex flex-col p-3 py-5 bg-gray-900 rounded-lg text-neutral-content mb-1">
            <span className="countdown font-mono text-5xl">
              <span style={{ "--value": `${minutes}` }}></span>
            </span>
          </div>
          Minutes
        </div>
        <div className="font-semibold">
          <div className="flex flex-col p-3 py-5 bg-gray-900 rounded-lg text-neutral-content mb-1">
            <span className="countdown font-mono text-5xl">
              <span style={{ "--value": `${seconds}` }}></span>
            </span>
          </div>
          Seconds
        </div>
      </div>
    );
  };

  return <div>{getFormattedTime(time)}</div>;
};

export default CountDown;
