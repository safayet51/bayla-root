"use client";

import { Dialog } from "primereact/dialog";
import { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";

const ContactModal = ({ visible, setVisible }) => {
  const [verified, setVerified] = useState(false);
  const [data, setData] = useState({
    name: "",
    factory: "",
    email: "",
    phone: "",
  });
  const onSubmit = () => {
    console.log(data);
  };
  function onChange(value) {
    console.log("Captcha value:", value);
    setVerified;
  }

  return (
    <Dialog
      visible={visible}
      onHide={() => setVisible(false)}
      breakpoints={{ "960px": "75vw", "641px": "100vw" }}
      style={{
        width: "50vw",
        boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
        background: "white",
        borderBottom: "2px solid gray",
      }}>
      <div className="max-w-4xl mx-auto">
        <div className="px-7 py-10">
          <form className="flex flex-col gap-7 mx-8">
            <div>
              <label className="label">Your Name</label>
              <input
                className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full"
                onChange={(e) => {
                  setData((prev) => ({ ...prev, name: e.target.value }));
                }}
              />
            </div>
            <div>
              <label className="label">Factory Name</label>
              <input
                className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full"
                onChange={(e) => {
                  setData((prev) => ({ ...prev, factory: e.target.value }));
                }}
              />
            </div>
            <div>
              <label className="label">Your Email</label>
              <input
                className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full"
                onChange={(e) => {
                  setData((prev) => ({ ...prev, email: e.target.value }));
                }}
              />
            </div>
            <div className="mb-5">
              <label className="label">Phone Number</label>
              <input
                className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full"
                onChange={(e) => {
                  setData((prev) => ({ ...prev, phone: e.target.value }));
                }}
              />
            </div>

            <ReCAPTCHA
              sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
              onChange={onChange}
            />

            <input
              type="submit"
              value="Send Now"
              className="bg-[#2c9244] font-medium text-white rounded-lg w-40 py-2 px-3"
              onClick={() => {
                onSubmit();
              }}
              disabled={!verified}
            />
          </form>
        </div>
      </div>
    </Dialog>
  );
};

export default ContactModal;
