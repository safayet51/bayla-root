"use client";

import CountDown from "./CountDown";
import { useState } from "react";
import ContactModal from "./ContactModal";

const YouthBanner = () => {
  const [visible, setVisible] = useState(false);
  return (
    <div
      className="hero min-h-screen"
      style={{
        backgroundImage:
          "url(https://i.ibb.co/3hKgNzB/Youth-Conference-Background.png)",
        marginTop: "-70px",
      }}>
      <div className="hero-overlay bg-opacity-60"></div>
      <div className="hero-content text-center text-neutral-content">
        <div className="max-w-5xl mt-20">
          <h1 className="mb-10 text-5xl font-bold text-center leading-[4rem] text-white">
            Empowering Tomorrow's Leaders Today: Join the BAYLA Youth
            Conference!
          </h1>
          <p className="text-[20px] text-white font-medium mb-2 mt-2 text-2xl leading-relaxed">
            Embark on a journey where Knowledge meets Innovation,
          </p>
          <p className="text-[20px] text-white font-medium mb-5 text-2xl">
            {" "}
            where Research thrives & Inclusion blossoms – all at the BAYLA Youth
            Conference!
          </p>
          <p className="text-[20px] text-white font-medium mb-4 text-2xl mt-12">
            Next Conference will start on
          </p>
          <div className="flex items-center justify-center mb-5">
            <CountDown duration={2 * 24 * 60 * 60 * 1000} />
          </div>

          <button
            className="font-semibold text-xl px-10 py-3 bg-[#2c9244] rounded-full"
            onClick={() => setVisible(true)}>
            {" "}
            BOOK YOUR SEAT
          </button>
        </div>
      </div>
      <ContactModal setVisible={setVisible} visible={visible} />
    </div>
  );
};

export default YouthBanner;
