"use client"
import Image from "next/image"
import img0 from "../../../assets/banner/BAYLA Privilege Card.png"
const PerkLeft = () => {
    return (
        <div className="flex flex-col items-center mt-2">
            <div className="border-dashed border-2 border-green-600 p-2 rounded-3xl">
                <Image src={img0} alt="" width={300} />
            </div>
            <div className="flex flex-col justify-center items-center my-6">
                <p className="text-xl mt-2">Be a Proud BAYLA Member</p>
                <p className="text-xl mb-5">to avail these priviliges</p>
                <button className="px-5 py-3 text-xl bg-[#5aa958] rounded-xl text-white">Become a Member</button>
            </div>
        </div>
    )
}

export default PerkLeft