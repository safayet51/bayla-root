"use client"

import Image from "next/image"

const PerksCard = ({ e }) => {

    return (
        <div className="border-dashed border-2 border-green-600 p-7 rounded-3xl m-8">
            <div className="grid grid-cols-2 justify-items-start items-center">
                <Image src={e.image} alt="image" />
                <p className="font-bold">{e?.name}</p>
            </div>
            <div className="grid grid-cols-2 justify-items-start items-center my-3">
                <p>DISCOUNT</p>
                <p>EXPIRES IN</p>
            </div>
            <div className="grid grid-cols-2 justify-items-start items-center my-1">
                <p className="text-6xl text-black font-bold">{e?.percent}</p>
                <div>
                    <p className="font-bold mb-1">{e?.date}</p>
                    <p className="px-3 py-1 border border-pink-400 rounded-md text-pink-400">{e?.week}</p>
                </div>
            </div>
        </div>
    )
}

export default PerksCard