"use client";

import Button from "@/components/shared/Button";
import Image from "next/image";
import Link from "next/link";

const PublicationCard = ({ elatest }) => {
  return (
    <Link href={`/publications/${elatest?.id}`}>
      <div
        className="w-[640ox] h-[630px] lg:h-[400px] flex flex-col lg:flex-row justify-center items-center lg:justify-normal lg:items-start bg-base-100 shadow-xl mt-7 hover:bg-green-500 hover:text-white rounded-xl relative"
        style={{
          boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
        }}>
        <div className="w-[320px] p-3">
          {" "}
          <Image
            src={elatest?.image}
            alt="Album"
            className="object-cover w-full h-full rounded-lg"
          />
        </div>
        <div className="w-[320px] p-3">
          <p className="m-0">{elatest?.tag}</p>
          <h2 className="text-xl lg:text-3xl font-bold mt-1 lg:mt-5 pr-0 lg:pr-3">
            {elatest?.title}
          </h2>

          <p className="mt-0 m-0 lg:mt-4">{elatest?.edition}</p>
          <p className="mt-0 m-0 lg:mt-4">{elatest?.date}</p>

          <div className="card-actions justify-start mt-4 absolute bottom-4">
            <Button
              title={"READ IT NOW"}
              radius={10}
              paddingX={15}
              paddingY={7}
              bgColor={"white"}
              borderColor={"#5aa958"}
              color={"#5aa958"}
            />
          </div>
        </div>
      </div>
    </Link>
  );
};

export default PublicationCard;
