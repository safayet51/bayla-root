"use client";
import logo from "../../../../assets/logo/Featured Content Logo.png";
import Image from "next/image";
import FeaturedCard from "./FeaturedCard";
import { useEffect, useState } from "react";
import axios from "axios";
import { motion } from "framer-motion";
const FeaturedContent = () => {
  const [cardContents, setCardContents] = useState();

  console.log("cardContents", cardContents);
  useEffect(() => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "get",
        collection: "blog",
        data: {},
        params: {},
      })
      .then((res) => {
        console.log("res?.data", res?.data);
        setCardContents(res?.data);
      });
  }, []);
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.9, delay: 0.9 }}
        className="flex flex-col justify-center items-center mt-32 mb-5">
        <Image src={logo} alt="leaderboard logo" width={50} />
        <h2 className="font-[800] text-[1.25rem] lg:text-4xl border-b-2 border-[#2C9244] my-6">
          FEATURED CONTENTS
        </h2>
      </motion.div>
      <div className="grid grid-cols-1 lg:grid-cols-2 w-[83.5%] lg:w-2/3 mx-auto items-center mt-12 gap-9 mb-16">
        {cardContents?.slice(0, 4)?.map((content, index) => (
          <FeaturedCard key={index} content={content} />
        ))}
      </div>
    </>
  );
};

export default FeaturedContent;
