"use client";
import Image from "next/image";
import Link from "next/link";
import { useParams } from "next/navigation";

const FeaturedCard = ({ content }) => {
  //   console.log(content);

  const { id } = useParams();

  return (
    <div className="h-[340px] md:h-[380px] lg:h-[450px] rounded-xl bg-base-100 shadow-xl border border-[#909090]">
      <div className="h-[180px] md:h-[210px] lg:h-[300px] w-full">
        {" "}
        <Image
          src={content.filename}
          alt="Shoes"
          width={500}
          height={300}
          className="h-full w-full object-cover rounded-b-none rounded-t-xl "
        />
      </div>
      <div className="p-3 mt-4">
        <div className="flex flex-col lg:flex-row items-center justify-between gap-2 lg:gap-5">
          {content?.title?.length > 50 ? (
            <p className="text-[#505050] font-medium text-center lg:text-start lg:text-[19px] lg:mt-3">
              {content?.title.slice(0, 50) + "..."}
            </p>
          ) : (
            <p className="text-[#505050] font-medium text-center lg:text-start lg:text-[19px] lg:mt-3">
              {content?.title}
            </p>
          )}

          <Link href={`/contents/${content?._id}`}>
            {" "}
            <button className="rounded-full px-4 py-1 lg:px-7 lg:py-2 hover:bg-green-800 bg-[#2C9244] text-white">
              Promote
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FeaturedCard;
