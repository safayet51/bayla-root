"use client";

import baylaLogo from "../../../assets/logo/BAYLALogo.png";
import Image from "next/image";
import { motion } from "framer-motion";
const Hero = () => {
  return (
    <div className="flex flex-col justify-center items-center mt-14">
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.5, delay: 0.4 }}>
        <Image
          src={baylaLogo}
          alt="baylaLogo"
          className="w-[40px] lg:w-[80px] md:w-[65px]"
        />
      </motion.div>

      <motion.p
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.5, delay: 0.5 }}
        className="mt-8 text-[14px] md:text-[18px] lg:text-[23px] font-semibold text-[#505050]">
        Join the Movement of
      </motion.p>
      <motion.h2
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.6, delay: 0.6 }}
        className="text-[18px] md:text-4xl lg:text-5xl font-bold mt-2 md:mt-4 lg:mt-6 text-[#181818]">
        #BRANDING
        <span className="text-[#2C9244] font-extrabold">BANGLADESH</span>
      </motion.h2>
      <motion.p
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.7, delay: 0.7 }}
        className="text-[14px] md:text-[18px] lg:text-[20px] mt-3 md:mt-5 lg:mt-7 font-semibold text-[#505050]">
        Encouraging - Enabling - Promoting
      </motion.p>
      <motion.p
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.7, delay: 0.7 }}
        className="text-[14px] md:text-[18px] lg:text-[20px] mt-0 lg:mt-[6px] font-semibold text-[#505050] text-center">
        The Good Practices of Bangladesh Apparel Manufacturing
      </motion.p>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.8, delay: 0.8 }}
        className="relative z-10">
        <h1
          className="text-7xl md:text-[100px] lg:text-[160px] font-extrabold mt-9 text-[#181818]"
          style={{ textShadow: "0px 4px 4px #282828" }}>
          239,454
        </h1>
        <p className="text-[14px] md:text-[18px] lg:text-[20px] absolute bottom-[-20px] lg:bottom-[-12px] right-0 font-semibold text-[#505050]">
          Audience reached
        </p>
      </motion.div>
    </div>
  );
};

export default Hero;
