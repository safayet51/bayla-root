import { useState } from "react";
import PromoteModal from "./PromoteModal";
import axios from "axios";

const ThirdPromot = ({ setToggle, formValues, onSubmit, selectedAmount }) => {
  const [visible, setVisible] = useState(false);
  const totalSelected = selectedAmount * 2000;
  const totalOther = formValues?.otherAmount ? formValues.otherAmount * 2 : 0;

  const handleNextClick = () => {
    onSubmit();
    setVisible(true);

    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "create",
        collection: "promote",
        data: {
          audiences: selectedAmount,
          otherAmount: formValues.otherAmount,
          displayName: formValues.displayName,
          email: formValues.email,
          companyName: formValues.companyName,
          mobileNumber: formValues.mobileNumber,
          totalSelected: totalSelected,
          totalOther: totalOther,
        },
        params: {},
      })
      .then((res) => {
        console.log("res", res.data);
      });
  };

  const handleGoBackClick = (e) => {
    e.preventDefault();
    setToggle(2);
  };

  return (
    <div className="flex flex-col justify-center items-center">
      <div className="mx-auto mt-6 font-medium">
        {formValues?.otherAmount ? (
          <p className="text-start">
            You are promoting to = {formValues?.otherAmount} audiences
          </p>
        ) : (
          <p className="text-start">
            You are promoting to = {selectedAmount},000 audiences
          </p>
        )}

        <p className="text-start">Cost to reach 1 audience = BDT 2.00</p>
      </div>
      <p className="text-center mt-6 font-medium">Total payable in BDT</p>
      {formValues?.otherAmount ? (
        <p className="text-center text-5xl font-extrabold text-[#2C9244]">
          {totalOther}
        </p>
      ) : (
        <p className="text-center text-5xl font-extrabold text-[#2C9244]">
          {totalSelected}
        </p>
      )}

      <button
        className="bg-[#2C9244] px-5 py-3 rounded-full text-white mt-8 text-xl w-full font-bold"
        onClick={handleNextClick}>
        Submit
      </button>

      <a
        href="#"
        className="underline font-medium text-center mt-4 text-[#606060]"
        onClick={handleGoBackClick}>
        Go back
      </a>

      <PromoteModal
        setVisible={setVisible}
        visible={visible}
        setToggle={setToggle}
      />
    </div>
  );
};

export default ThirdPromot;
