"use client";

import { useState } from "react";

const SecondPromote = ({ setToggle, formValues, setFormValues, onSubmit }) => {
  const [termsChecked, setTermsChecked] = useState(false);
  const [updatesChecked, setUpdatesChecked] = useState(false);

  // const handleNextClick = () => {
  //   if (
  //     value.displayName !== "" &&
  //     value.email !== "" &&
  //     value.companyName !== "" &&
  //     value.mobileNumber !== "" &&
  //     termsChecked &&
  //     updatesChecked
  //   ) {
  //     setToggle(3);
  //   }
  // };

  const handleNextClick = () => {
    // Assuming you want to update formValues with the input values
    setFormValues((prev) => ({
      ...prev,
      displayName: formValues.displayName,
      email: formValues.email,
      companyName: formValues.companyName,
      mobileNumber: formValues.mobileNumber,
    }));

    // Perform any additional logic if needed
    onSubmit();

    // Move to the next step
    setToggle(3);
  };

  return (
    <div className="flex flex-col">
      <div className="mb-4">
        <p className="text-center font-medium text-2xl">Details</p>
      </div>
      <form>
        <label className="text-start">DISPLAY NAME</label>
        <input
          type="text"
          name="displayname"
          placeholder="Display Name"
          value={formValues.displayName}
          onChange={(e) => {
            setFormValues((prev) => ({ ...prev, displayName: e.target.value }));
          }}
          className="input input-bordered border-gray-900 w-full h-16 mb-5 mt-1"
        />
        <label className="text-start">EMAIL Address</label>
        <input
          type="email"
          name="email"
          placeholder="Email Address"
          value={formValues.email}
          onChange={(e) => {
            setFormValues((prev) => ({ ...prev, email: e.target.value }));
          }}
          className="input input-bordered border-gray-900 w-full h-16 mb-5 mt-1"
        />
        <label className="text-start">COMPANY NAME</label>
        <input
          type="text"
          name="companyName"
          placeholder="Company Name"
          className="input input-bordered border-gray-900 w-full h-16 mb-5 mt-1"
          value={formValues.companyName}
          onChange={(e) => {
            setFormValues((prev) => ({ ...prev, companyName: e.target.value }));
          }}
        />
        <label className="text-start">MOBILE NUMBER</label>
        <input
          type="text"
          name="mobileNumber"
          value={formValues.mobileNumber}
          onChange={(e) => {
            setFormValues((prev) => ({
              ...prev,
              mobileNumber: e.target.value,
            }));
          }}
          placeholder="Mobile Number"
          className="input input-bordered border-gray-900 w-full h-16 mt-1"
        />
      </form>

      <div className="mt-7">
        {/* <div className="flex justify-center items-center gap-2 mt-3">
          {" "}
          <input
            type="checkbox"
            checked={termsChecked}
            onChange={() => setTermsChecked(!termsChecked)}
          />{" "}
          <p>I accept terms & conditions and policies.</p>
        </div> */}
        <div className="flex justify-center items-center gap-2 mt-3">
          <input
            type="checkbox"
            checked={updatesChecked}
            onChange={() => setUpdatesChecked(!updatesChecked)}
          />{" "}
          <p>
            {" "}
            <span className="text-[#2C9244] font-bold mb-1">YES!</span> I want
            regular updates on #BRANDINGBANGLADESH
          </p>
        </div>
      </div>

      {/* <button
        className={`${
          value.displayName === "" ||
          value.email === "" ||
          value.companyName === "" ||
          value.mobileNumber === "" ||
          !termsChecked ||
          !updatesChecked
            ? "bg-gray-300 text-[#889c8d] cursor-not-allowed"
            : "bg-[#2C9244]"
        } px-5 py-3 rounded-full w-full text-white mt-8 text-xl font-bold`}
        onClick={handleNextClick}
        disabled={
          value.displayName === "" ||
          value.email === "" ||
          value.companyName === "" ||
          value.mobileNumber === "" ||
          !termsChecked ||
          !updatesChecked
        }>
        Next
      </button> */}

      <button
        className={`${
          formValues.displayName === "" ||
          formValues.email === "" ||
          formValues.companyName === "" ||
          formValues.mobileNumber === "" ||
          !termsChecked ||
          !updatesChecked
            ? "bg-gray-300 text-[#889c8d] cursor-not-allowed"
            : "bg-[#2C9244]"
        } px-5 py-3 rounded-full w-full text-white mt-8 text-xl font-bold`}
        onClick={handleNextClick}
        disabled={
          formValues.displayName === "" ||
          formValues.email === "" ||
          formValues.companyName === "" ||
          formValues.mobileNumber === "" ||
          !termsChecked ||
          !updatesChecked
        }>
        Next
      </button>

      <a
        href="#"
        className="underline font-medium text-center mt-4 text-[#606060]"
        onClick={() => setToggle(1)}>
        Go back
      </a>
    </div>
  );
};

export default SecondPromote;
