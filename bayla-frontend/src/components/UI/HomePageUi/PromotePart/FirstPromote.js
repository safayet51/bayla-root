const FirstPromote = ({
  setToggle,
  selectedAmount,
  formValues,
  setFormValues,
  onSubmit,
}) => {
  const handleNextClick = () => {
    setFormValues((prev) => ({ ...prev, audiences: selectedAmount }));
    onSubmit();
    setToggle(2);
  };

  return (
    <div className="flex flex-col justify-center items-center mt-3">
      <button
        className={`${
          selectedAmount !== null || formValues.otherAmount
            ? "bg-[#2C9244] text-white"
            : "bg-gray-300 text-[#889c8d] cursor-not-allowed"
        } px-5 py-3 rounded-full w-full mt-8 text-xl font-bold`}
        onClick={handleNextClick}
        disabled={!(selectedAmount !== null || formValues.otherAmount)}>
        Next
      </button>
    </div>
  );
};

export default FirstPromote;
