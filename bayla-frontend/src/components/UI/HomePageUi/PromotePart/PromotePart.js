"use client";
import { useEffect, useState } from "react";
import FirstPromote from "./FirstPromote";
import SecondPromote from "./SecondPromote";
import ThirdPromot from "./ThirdPromot";
import { motion } from "framer-motion";
const PromotePart = () => {
  const [toggle, setToggle] = useState(1);
  const [selectedAmount, setSelectedAmount] = useState(null);
  const [formValues, setFormValues] = useState({
    audiences: selectedAmount,
    otherAmount: "",
    displayName: "",
    email: "",
    companyName: "",
    mobileNumber: "",
  });

  // Additional state to manage form submission status
  const [formSubmitted, setFormSubmitted] = useState(false);

  useEffect(() => {
    console.log(toggle);
  }, [toggle]);

  const handleAmountClick = (amount) => {
    setSelectedAmount(amount);
  };

  // Handle form submission across steps
  const handleSubmit = () => {
    // Implement your logic for submitting the form data
    // You can make API calls, update state, etc.
    console.log("Form Submitted:", formValues);
    setFormSubmitted(true);
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.8, delay: 0.8 }}
      className="mt-36 flex flex-col justify-center items-center mb-36 pt-32 md:pt-48 lg:pt-64">
      <div
        className="card w-[341px] md:w-[530px] lg:w-[620px] shadow-xl bg-base-100 mx-5"
        style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px" }}>
        <div className="card-body p-4 lg:card-body mx-4">
          <h2 className="text-center font-semibold text-[18px] md:text-[22px] lg:text-2xl">
            Promote to
          </h2>
          <div className="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-3 gap-4 mt-6">
            {[10, 20, 50].map((amount) => (
              <div key={amount}>
                <h2
                  onClick={() => handleAmountClick(amount)}
                  className={`px-3 py-3 text-center bg-gray-200 border cursor-pointer hover:bg-[#5aa958] hover:border-none hover:text-white  border-[#909090] rounded-md font-medium ${
                    selectedAmount === amount
                      ? "bg-green-500 text-white border-none"
                      : ""
                  }`}>
                  {" "}
                  <span className="lg:text-2xl md:text-[22px] font-semibold pr-[2px]">
                    {amount}K
                  </span>
                  audiences
                </h2>
              </div>
            ))}
          </div>
          <div className="flex items-center mt-5">
            <div className="flex-1 border-t border-gray-400"></div>
            <span className="px-3 text-gray-500 bg-white">OR</span>
            <div className="flex-1 border-t border-gray-400"></div>
          </div>
          <input
            type="text"
            value={formValues.otherAmount}
            onChange={(e) => {
              setFormValues((prev) => ({
                ...prev,
                otherAmount: e.target.value,
              }));
            }}
            placeholder="Other amount"
            className="input input-bordered border-[#909090] w-full mt-5 h-16 mb-12"
          />

          {toggle === 2 ? (
            <SecondPromote
              setToggle={setToggle}
              formValues={formValues}
              setFormValues={setFormValues}
              onSubmit={handleSubmit}
            />
          ) : toggle === 3 ? (
            <ThirdPromot
              selectedAmount={selectedAmount}
              setToggle={setToggle}
              formValues={formValues}
              setFormValues={setFormValues}
              onSubmit={handleSubmit}
            />
          ) : (
            <FirstPromote
              setToggle={setToggle}
              selectedAmount={selectedAmount}
              formValues={formValues}
              setFormValues={setFormValues}
              onSubmit={handleSubmit}
            />
          )}
        </div>
      </div>
    </motion.div>
  );
};

export default PromotePart;
