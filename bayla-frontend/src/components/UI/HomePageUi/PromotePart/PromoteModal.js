import { Dialog } from "primereact/dialog";
import React from "react";
import img from "../../../../assets/banner/Success Icon.png";
import Image from "next/image";
import { FaFacebookSquare, FaInstagram, FaLinkedin } from "react-icons/fa";
const PromoteModal = ({ visible, setVisible, setToggle }) => {
  return (
    <Dialog
      header=""
      visible={visible}
      style={{ width: "50vw", position: "relative" }}
      onHide={() => setVisible(false)}>
      <div className="flex flex-col justify-center items-center">
        <Image src={img} width={200} alt="" className="mx-auto" />
        <p className="font-medium mt-2 text-2xl text-black">
          Thank you for participating in Branding Bangladesh
        </p>

        <p className="font-medium my-1 text-2xl text-black">
          We will update you with your impact matrics soon
        </p>
        <p className="mt-8">#ForBrandingBangladesh contents</p>
        <p className="mt-3">Follow us on</p>

        <div className="flex justify-center items-center w-32 mx-auto mt-1 text-2xl gap-3">
          <a href="https://www.facebook.com/baylaorg" target="_blank">
            <FaFacebookSquare className="text-blue-600" />
          </a>
          <a href="https://www.instagram.com/baylaorg/" target="_blank">
            <FaInstagram className="text-pink-500" />
          </a>
          <a
            href="https://www.linkedin.com/company/baylaorg/about/"
            target="_blank">
            <FaLinkedin className="text-blue-800" />
          </a>
        </div>

        <p
          className="bg-white hover:bg-gray-600 hover:text-white p-3 rounded-full absolute top-0 right-2 cursor-pointer"
          onClick={() => {
            setVisible(false);
            setToggle(1);
          }}>
          OK
        </p>
      </div>
    </Dialog>
  );
};

export default PromoteModal;
