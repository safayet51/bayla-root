"use client"
import BackImage from "../../../../assets/banner/Best Promoter Background.png"
import profileImage from "../../../../assets/logo/profile.jpeg";
import Image from "next/image";
import badge from "../../../../assets/logo/Best Promoter Badge.png"
export function LeaderCard() {
    return (
        <div className="relative mt-12">
            <div className="avatar absolute top-[-8%] border-deep-orange-300 left-[360px] z-10">
                <div className="w-32 rounded-full">
                    <Image src={profileImage} alt="" className="w-8" />
                </div>
            </div>
            <div className="absolute z-10 top-10 left-[445px]">
                <Image src={badge} alt="" className="w-8" />
            </div>
            <div className="w-[840px] relative">
                <p className="absolute top-20 mt-5 left-[330px] font-semibold text-[#1D6FB1]">Promoter of the month</p>
                <div className="absolute top-[180px] left-[200px]">
                    <p className="font-bold text-[18px]">Full Name</p>
                    <p className="mt-1">Company Name</p>
                </div>
                <div className="absolute top-[180px] left-[530px]">
                    <p className="font-bold text-[16px] text-white px-5 py-1 bg-[#1D6FB1] rounded-full">185K people</p>
                    <p className="mt-1 ml-7">dd/mm/yyy</p>
                </div>
                <Image src={BackImage} alt="Shoes" className="w-full object-cover" />
            </div>




        </div>


    );
}