"use client"

import Image from "next/image"
import leaderBoardLogo from "../../../../assets/logo/Leaderboard Logo.png"
import profile1 from "../../../../assets/logo/profile2.jpeg";
import profile2 from "../../../../assets/logo/profile1.jpeg";
import profile3 from "../../../../assets/logo/profile4.jpeg";
import profile4 from "../../../../assets/logo/profile3.jpeg";
import { LeaderCard } from "./LeaderCard"
import OtherCard from "./OtherCard"
import Button from "@/components/shared/Button";

const LeaderBoard = () => {

    const personDetails = [
        {
            name: "Full Name",
            company: "Company Name",
            peopleView: "100K people",
            date: "dd/mm/yyy",
            image: profile1
        },
        {
            name: "Full Name",
            company: "Company Name",
            peopleView: "100K people",
            date: "dd/mm/yyy",
            image: profile2
        },
        {
            name: "Full Name",
            company: "Company Name",
            peopleView: "100K people",
            date: "dd/mm/yyy",
            image: profile3
        },
        {
            name: "Full Name",
            company: "Company Name",
            peopleView: "100K people",
            date: "dd/mm/yyy",
            image: profile4
        }
    ]
    return (
        <div className="flex flex-col justify-center items-center mt-10">
            <Image src={leaderBoardLogo} alt="leaderboard logo" width={50} />
            <h2 className="font-[800] text-2xl border-b-2 border-green-500 mt-6">LEADERBOARD</h2>

            <div className="mt-10">
                <LeaderCard />
            </div>
            <div className="mt-3 w-[756px]">
                {
                    personDetails?.map((e, i) => <OtherCard e={e} key={i} />)
                }
                <div className="mt-12">
                    <Button
                        onClick={() => setToggle(2)}
                        bgColor={"#5aa958"}
                        paddingY={13}
                        paddingX={3}
                        radius={50}
                        color={"#fff"}
                        title={"Promote Now"}
                        width={100}
                        fontWeight={600}
                        fontSize={20} />
                </div>

            </div>
        </div>
    )
}

export default LeaderBoard