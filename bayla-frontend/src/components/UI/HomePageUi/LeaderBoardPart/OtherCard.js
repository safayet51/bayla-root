"use client"
import Image from 'next/image';
import star from "../../../../assets/logo/Leaderboard Badge.png"

const OtherCard = ({ e }) => {


    return (
        <div className='grid grid-cols-2 justify-items-stretch rounded-lg relative px-5 items-center py-4 mt-5 transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-10' style={{ boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px" }}>
            <div className='flex items-center justify-start gap-5'>
                <div className='relative'>
                    <div className="avatar border-deep-orange-300 z-10">
                        <div className="w-16 rounded-full">
                            <Image src={e?.image} alt="" className="w-8" />
                        </div>
                    </div>
                    <Image src={star} alt="" width={40} className='absolute left-10 top-8 z-10' />
                </div>
                <div>
                    <p className='font-bold text-[18px]'>{e?.name}</p>
                    <p className="mt-1">{e?.company}</p>
                </div>
            </div>
            <div className='ml-auto'>
                <button className="font-bold text-[16px] text-white px-5 py-1 bg-[#5AA958] rounded-full"> {e?.peopleView}
                </button>
                <p className="mt-1 ml-6">{e?.date} </p>

            </div>
        </div>
    )
}

export default OtherCard;