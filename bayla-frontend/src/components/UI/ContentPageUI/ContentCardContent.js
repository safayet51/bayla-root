"use client";
import { useEffect, useState } from "react";
import FeaturedCard from "../HomePageUi/FeaturedContentPart/FeaturedCard";
import axios from "axios";

const ContentCardContent = () => {
  const [cardContents, setCardContents] = useState();
  console.log("cardContents", cardContents);
  useEffect(() => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "get",
        collection: "blog",
        data: {},
        params: {},
      })
      .then((res) => setCardContents(res.data));
  }, []);
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 w-[83.5%] lg:w-2/3 mx-auto items-center mt-12 gap-9 mb-10">
      {cardContents?.map((content, index) => (
        <FeaturedCard key={index} content={content} />
      ))}
    </div>
  );
};

export default ContentCardContent;
