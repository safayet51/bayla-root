"use client"
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import img1 from "../../../assets/contents/Top Content.png"
import Image from "next/image";
const ContentCarousel = () => {

    return (
        <div className="mt-5 max-w-7xl mx-auto">
            <Carousel autoPlay infiniteLoop>
                <div className="relative ">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>
                <div className="relative">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>
                <div className="relative">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>
                <div className="relative">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>
                <div className="relative">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>
                <div className="relative">
                    <Image src={img1} alt="banner" className="w-full" />

                </div>

            </Carousel>
        </div>
    )
}

export default ContentCarousel;