"use client"
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import Image from "next/image";
import img1 from "../../../assets/bayla/man1.jpg";
import img2 from "../../../assets/bayla/Quote Logo.png"
const Community = () => {
    return (
        <div className="max-w-7xl mx-auto">

            <div className="grid grid-cols-2 gap-20 mt-28 max-w-6xl mx-auto">
                <div className="text-xl">
                    <h>From our </h>
                    <h2 className="font-bold text-3xl">Community...</h2>
                    <p className="mt-6">Here's what our well-wishers </p>
                    <p className="mb-12">had to say about BAYLA</p>
                    <button className="px-5 py-2 text-green-600 border border-green-500 rounded-xl ">Give us your review</button>

                </div>
                <div className="relative">
                    <div className="mt-5 max-w-6xl mx-auto">
                        <Carousel autoPlay infiniteLoop>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-5">Demo Name</p>
                                    <p>CEO, Patronum</p>


                                </div>
                                <div className="text-justify mt-5 flex gap-2 justify-normal items-start"> <Image src={img2} className="w-10" /> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</div>

                            </div>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-8">Demo Name</p>
                                    <p>CEO, Patronum</p>


                                </div>
                                <p className="text-justify mt-5"> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</p>
                            </div>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-8">Demo Name</p>
                                    <p>CEO, Patronum</p>

                                </div>
                                <p className="text-justify mt-5"> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</p>
                            </div>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-8">Demo Name</p>
                                    <p>CEO, Patronum</p>

                                </div>
                                <p className="text-justify mt-5"> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</p>
                            </div>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-8">Demo Name</p>
                                    <p>CEO, Patronum</p>

                                </div>
                                <p className="text-justify mt-5"> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</p>
                            </div>
                            <div className="relative">
                                <div className="flex flex-col justify-center items-center ">
                                    <div className="avatar">
                                        <div className="w-32 rounded-full">
                                            <Image src={img1} alt="banner" className="w-full" />
                                        </div>
                                    </div>
                                    <p className="font-medium mt-8">Demo Name</p>
                                    <p>CEO, Patronum</p>

                                </div>
                                <p className="text-justify mt-5"> Le quartier est un endroit idéal pour aller
                                    après un match des Padres, pour quelques
                                    verres après le travail ou simplement pour
                                    manger un morceau. Bonne nourriture et
                                    bières géniales de mour preatasu aller.</p>
                            </div>

                        </Carousel>
                    </div>

                </div>

            </div>
        </div>
    )
}

export default Community