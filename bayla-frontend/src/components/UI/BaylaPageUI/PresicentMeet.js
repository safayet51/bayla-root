"use client";

import Image from "next/image";
import baylaBackground from "../../../assets/bayla/president_image 1.png";
import { useState } from "react";
import FullMessageModal from "./FullMessageModal";

const PresicentMeet = () => {
  const [visible, setVisible] = useState(false);
  return (
    <div className="max-w-7xl mx-auto my-20">
      <div className="relative">
        <h2 className="font-bold text-center my-24 text-3xl">
          Meet the President
        </h2>
      </div>
      <div className="grid grid-cols-2 gap-20 mt-28 max-w-7xl mx-auto justify-items-center justify-center items-center">
        <div className="relative">
          <Image src={baylaBackground} alt="" width={600} />
        </div>
        <div>
          <h2 className="font-bold text-3xl">Abrar Hossain Sayem</h2>
          <p className="font-medium">President</p>
          <p className="font-medium">president@bayla.org</p>
          <p className="font-medium text-xl text-justify mt-10 leading-relaxed">
            To face the challenges and grab the opportunities of the fourth
            industrial revolution young leaders of the industry came to mutual
            consent on jointly increasing their effort and thus forming a
            youth-based association called BAYLA. This association will work
            closely with leading associations of the industry like BGMEA, BKMEA,
            BTMA and BGAPMEA.
          </p>
          <p className="font-medium text-xl text-justify mt-10 leading-relaxed">
            This unique proposal of this body is to research innovation, bring
            youth leaders together, finding new opportunities for training,
            capacity building, and market expansion. The team is expected to be
            contributing significantly to the apparel industry of Bangladesh.
            This is solely a non-political association born out of the
            motivation and{" "}
            <span
              className="underline text-[#2C9244] font-medium cursor-pointer"
              onClick={() => setVisible(true)}>
              Full Message
            </span>
          </p>
          <FullMessageModal setVisible={setVisible} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default PresicentMeet;
