"use client";

import { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
const ContactForm = () => {
  const [verified, setVerified] = useState(false);
  const [data, setData] = useState({
    name: "",
    factory: "",
    email: "",
    phone: "",
  });
  const onSubmit = () => {
    console.log(data);
  };
  function onChange(value) {
    setVerified;
  }
  return (
    <div
      className="max-w-7xl mx-auto py-18 my-14 pl-6 bg-base-200"
      id="contactForm">
      <div className="px-7 py-10  mx-12">
        <h3 className="text-4xl font-medium pb-14">
          Contact us to become a member ...
        </h3>

        <form className="flex flex-col gap-7">
          <div>
            <label className="label">Your Name</label>
            <input
              className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full "
              onChange={(e) => {
                setData((prev) => ({ ...prev, name: e.target.value }));
              }}
            />
          </div>
          <div>
            <label className="label">Factory Name</label>
            <input
              className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full "
              onChange={(e) => {
                setData((prev) => ({ ...prev, factory: e.target.value }));
              }}
            />
          </div>
          <div>
            <label className="label">Your Email</label>
            <input
              className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full"
              onChange={(e) => {
                setData((prev) => ({ ...prev, email: e.target.value }));
              }}
            />
          </div>
          <div className="mb-5">
            <label className="label">Phone Number</label>
            <input
              className="input !bg-transparent border-0 !border-b border-gray-600 rounded-none w-full "
              onChange={(e) => {
                setData((prev) => ({ ...prev, phone: e.target.value }));
              }}
            />
          </div>

          <ReCAPTCHA
            sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
            onChange={onChange}
          />

          <input
            type="submit"
            value="Send Now"
            className="bg-[#2c9244] font-medium text-white rounded-lg w-40 py-2 px-3 mt-3"
            onClick={() => {
              onSubmit();
            }}
            disabled={!verified}
          />
        </form>
      </div>
    </div>
  );
};

export default ContactForm;
