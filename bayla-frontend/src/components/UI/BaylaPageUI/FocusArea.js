"use client"

import Image from "next/image"
import baylaBackground from "../../../assets/bayla/BAYLA.png";
import baylaBackground1 from "../../../assets/bayla/KRII.png";
import baylaBackground2 from "../../../assets/bayla/Research.png";
import baylaBackground3 from "../../../assets/bayla/Innovation.png";
import baylaBackground4 from "../../../assets/bayla/Inclusion.png";
const FocusArea = () => {
    const focusImage = [
        {
            id: 1,
            img: baylaBackground1
        },
        {
            id: 2,
            img: baylaBackground2
        },
        {
            id: 3,
            img: baylaBackground3
        },
        {
            id: 4,
            img: baylaBackground4
        }
    ]
    return (
        <div className="grid grid-cols-2 gap-40 mt-28 max-w-7xl mx-auto justify-items-center justify-center items-center">
            <div>
                <h2 className="font-bold text-3xl">Focus areas</h2>
                <p className="font-medium mt-5 text-xl text-justify leading-relaxed">BAYLA, an association of young leaders in
                    the Bangladesh apparel industry, strives to
                    drive positive change through innovative
                    solutions, knowledge sharing, and industry
                    collaboration. By uniting youth leaders, BAYLA
                    aims to address challenges, explore growth
                    opportunities, and collectively shape a brighter
                    future for the apparel sector.</p>
            </div>
            <div className="relative">
                <Image src={baylaBackground} alt="" width={500} />
                <div className="absolute top-10 right-0">
                    <Image src={baylaBackground1} alt="" width={800} />
                </div>
            </div>
        </div>
    )
}

export default FocusArea