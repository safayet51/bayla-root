"use client"

import logo from "../../../assets/bayla/download.png";
import { useState } from "react";
import DialogModal from "@/components/shared/DialogModal";
import CardWithAverter from "@/components/shared/CardWithAverter";


const MemberFactories = () => {
    const factoryData = [
        {
            id: "1",
            name: "Factory Name",
            location: "Location",
            img: logo
        },
        {
            id: "2",
            name: "Factory Name",
            location: "Location",
            img: logo
        },
        {
            id: "3",
            name: "Factory Name",
            location: "Location",
            img: logo
        },
        {
            id: "4",
            name: "Factory Name",
            location: "Location",
            img: logo
        },
        {
            id: "5",
            name: "Factory Name",
            location: "Location",
            img: logo
        }
    ]
    const [visible, setVisible] = useState(false);
    return (
        <div className="max-w-7xl mx-auto">
            <div className="relative">
                <h2 className="font-bold text-center my-24 text-3xl">
                    Member Factories
                </h2>
                <p className="text-[#5aa958] underline font-medium absolute right-0 top-2 cursor-pointer" onClick={() => setVisible(true)} >See all Factories</p>
                <DialogModal setVisible={setVisible} visible={visible} />
            </div>

            <div className="grid grid-cols-5 gap-5 justify-center items-center text-center">
                {
                    factoryData?.map((e, i) => <CardWithAverter e={e} key={i} />)
                }
            </div>
        </div>
    )
}

export default MemberFactories