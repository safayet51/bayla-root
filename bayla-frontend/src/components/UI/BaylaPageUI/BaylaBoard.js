import CardWithAverter from "@/components/shared/CardWithAverter";
import { boardData } from "../../shared/data";

const BaylaBoard = () => {
  return (
    <div className="max-w-8xl">
      <div className="relative">
        <h2 className="font-bold text-center my-24 text-3xl">BAYLA Members</h2>
      </div>
      <div className="grid grid-cols-4 gap-5 max-w-7xl mx-auto mb-20">
        {boardData?.map((e, i) => (
          <CardWithAverter key={i} e={e} />
        ))}
      </div>
    </div>
  );
};

export default BaylaBoard;
