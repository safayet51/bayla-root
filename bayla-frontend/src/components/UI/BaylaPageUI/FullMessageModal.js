"use client"
import Image from "next/image"
import img1 from "../../../assets/bayla/Quote Logo.png"
import img2 from "../../../assets/logo/Presidents_Signature.png"
import img3 from "../../../assets/bayla/BAYLA  - Full Logo.png"
import { Dialog } from "primereact/dialog";
const FullMessageModal = ({ visible, setVisible }) => {

    return (
        <Dialog header="Message From President" visible={visible} onHide={() => setVisible(false)}
            breakpoints={{ '960px': '75vw', '641px': '100vw' }} style={{ width: '50vw', boxShadow: "rgba(0, 0, 0, 0.35) 0px 5px 15px", background: "white", borderBottom: "2px solid gray", }} >
            <div className="flex gap-2 justify-normal items-start mt-2 border-0 border-t pt-3">
                <Image src={img1} width={40} alt="" />
                <div className="text-[18px] text-justify pr-8 mt-5 leading-relaxed text-[#505050]">
                    <div className="font-medium">To face the challenges and grab the opportunities of the fourth industrial revolution
                        young leaders of the industry came to mutual consent on jointly increasing their effort
                        and thus forming a youth-based association called BAYLA (Bangladesh Apparel Youth
                        Leaders Association). This association will work closely with leading associations of the
                        industry like BGMEA, BKMEA, BTMA and BGAPMEA.</div>
                    <div className="mt-8 font-medium">This unique proposal of this body is to research innovation, bring youth leaders together,
                        finding new opportunities for training, capacity building, and market expansion. The team
                        is expected to be contributing significantly to the apparel industry of Bangladesh. This is
                        solely a non-political association born out of the motivation and determination of youth.
                        Any leader running an organization related to apparel manufacturing and is aged below
                        50 can be a part of the association. Along with the mentioned area of focus, BAYLA will
                        also work for branding the industry by promoting its innovation and sustainability.
                    </div>
                    <div className="mt-8 font-medium">Going forward, the industry demands a more synergistic approach in business. It is time
                        for reshaping and rethinking the future. When we talk about strategic moves to reshape
                        the future, young minds are undoubtedly the right asset. We believe that together we can
                        be strong and proactive.
                    </div>
                    <div className="mt-8 font-medium">Collectively youths of the industry will look for new ways of cross trade and technologies
                        to establish a robust technology centered business model which will be able to combat
                        urgent global challenges.
                    </div>
                    <div className="mt-8 font-medium">We believe, the required changes can be planned, formulated, and implemented by the
                        combination of experience and enthusiasm. We need our senior leaders who can guide
                        us with their experience and a rising group of young leaders who are enthusiastic about
                        the new changes, challenges, and opportunities to work together towards a singular goal.
                    </div>
                    <div className="flex justify-between items-center mt-16">
                        <div>
                            <Image src={img2} alt="" width={150} />
                            <h2 className="font-bold text-xl">Abrar Hossain Sayem</h2>
                            <p className="font-medium">President</p>
                            <p className="font-medium">president@bayla.org</p>
                        </div>
                        <div>
                            <Image src={img3} alt="" width={150} />
                        </div>
                    </div>
                </div>
            </div>
        </Dialog>
    )
}

export default FullMessageModal