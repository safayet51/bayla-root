"use client";

import baylaLogo from "../../../assets/bayla/BAYLA  - Full Logo.png";
import Image from "next/image";
import FocusArea from "./FocusArea";
import Link from "next/link";

const BaylaPageUI = () => {
  return (
    <div className="flex flex-col justify-center items-center mt-16">
      <Image src={baylaLogo} alt="baylaLogo" width={120} />
      <h2 className="text-[40px] font-bold mt-6">
        Bangladesh Apparel Youth Leaders Association (BAYLA)
      </h2>
      <p className="text-[22px] mt-8 font-semibold text-[#505050] break-words max-w-5xl mx-auto text-center">
        Second generation leaders driving change in Bangladesh apparel industry
      </p>
      <p className="text-[22px] mb-10 mt-1 font-semibold text-[#505050] break-words max-w-5xl mx-auto text-center">
        through Research, Innovation, Inclusion and Knowledge sharing.
      </p>
      <Link href="#contactForm">
        <button className="bg-[#2C9244] px-8 py-2 text-xl rounded-2xl text-white font-medium">
          CONTACT US
        </button>
      </Link>
      <FocusArea />
    </div>
  );
};

export default BaylaPageUI;
