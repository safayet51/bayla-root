"use client";
import { useState } from "react";
import AdminPageUi from "./AdminPageUi";

const Admin = () => {
  const secret = "1234";
  const [password, setPassword] = useState();
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  return (
    <>
      {!isAuthenticated && (
        <input
          placeholder="Enter Password"
          type="text"
          onChange={(e) => {
            if (e.target.value == secret) {
              setIsAuthenticated(true);
            }
          }}
        />
      )}
      {isAuthenticated && <AdminPageUi />}
    </>
  );
};

export default Admin;
