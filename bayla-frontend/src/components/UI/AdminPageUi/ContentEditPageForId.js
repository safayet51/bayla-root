"use client";

import { IoArrowBack } from "react-icons/io5";
import React, { useEffect, useState } from "react";

import "react-quill/dist/quill.snow.css";
import axios from "axios";
import Link from "next/link";
import Image from "next/image";
import dynamic from "next/dynamic";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
const ContentEditPageForId = ({ Id }) => {
  const [getIdData, setGetIdData] = useState();
  const [value, setValue] = useState("");
  const [file, setFile] = useState(null);
  const [title, setTitle] = useState();
  const [filename, setFilename] = useState("");
  const modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [{ list: "ordered" }, { list: "bullet" }],
      ["link", "image"],
      ["clean"],
    ],
  };

  const formats = [
    "header",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "link",
    "image",
  ];

  const handleImageUpload = (file) => {
    const imageUrl = "https://example.com/placeholder-image.jpg";
    const updatedValue = `${value}<img src="${imageUrl}" alt="Uploaded Image" />`;
    setValue(updatedValue);
  };
  const handleUpload = (file) => {
    const formData = new FormData();
    formData.append("file", file);

    axios
      .post(`${process.env.NEXT_PUBLIC_ATTACHMENT_API}`, formData)
      .then((res) => setFilename(res.data.filename))
      .catch(function (error) {
        console.log(error);
      });
  };
  const handleSubmit = () => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "create",
        collection: "blog",
        data: { title, value, filename },
        params: {},
      })
      .then((res) => {
        setTitle("");
        setValue("");
        setFile(null);
        setFilename("");
        // setToggle(1)
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    axios
      .post(`${process.env.NEXT_PUBLIC_LIVE_API}`, {
        action: "read",
        collection: "blog",
        data: {},
        params: { _id: Id },
      })
      .then((res) => {
        setGetIdData(res.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return (
    <div className="overflow-auto h-[90vh] pb-16 ">
      <div className="absolute top-10 left-76">
        <Link href="/admin/content">
          {" "}
          <div className="cursor-pointer flex justify-start items-center">
            <IoArrowBack />
            <p className="text-[15px]">Back</p>
          </div>
        </Link>
      </div>
      <div className="mt-32 mx-20">
        <div className="flex flex-col">
          <label className="pb-1">Contents Title:</label>
          <input
            type="text"
            placeholder="Type here"
            className="input input-bordered w-full !rounded-none"
            value={title}
            defaultValue={getIdData?.title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
          />
        </div>
        <div className="mt-7">
          <label className="">Featured Image:</label>
          <div className="border border-gray-500 p-2">
            <input
              type="file"
              multiple
              onChange={(e) => {
                handleUpload(e.target.files[0]);
                setFile(e.target.files[0]);
              }}
            />
            {getIdData?.filename ? (
              <Image
                src={getIdData?.filename}
                alt=""
                width={200}
                height={200}
              />
            ) : (
              ""
            )}
          </div>
          <div
            className="cursor-pointer rounded-[5px] bg-[#4E9345] text-white px-3 absolute top-24 right-24"
            onClick={handleSubmit}>
            <p className="text-[16px] font-medium">Update</p>
          </div>
        </div>{" "}
        <div className="mt-8">
          <ReactQuill
            theme="snow"
            value={getIdData?.value}
            onChange={setValue}
            modules={modules}
            formats={formats}
            placeholder="Write something..."
            bounds=".ql-editor"
          />
        </div>
      </div>
    </div>
  );
};

// export default ContentEditPageForId;
export default dynamic(() => Promise.resolve(ContentEditPageForId), {
  ssr: false,
});
